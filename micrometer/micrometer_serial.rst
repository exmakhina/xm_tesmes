#############################
Generic Serial SPC Micrometer
#############################


Most micrometers with SPC come with cables that act like serial ports and
output lines of measurement values, eg::

   +0.170
   +0.277
   +0.315
   +0.331
   +0.300
   +0.113
   -0.154
   -0.187
   -0.187
   -0.187
   -0.227
   -0.366
   -0.403

Example of supported devices:

- spcanywhere.com smart cables for micrometers

