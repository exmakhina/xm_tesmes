#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Generic micrometer implementation - for SPC cables

import io
import re
import time
import sys
import logging
import queue
import threading


with io.open(__file__.replace(".py", ".rst"), "r") as fi:
	__doc__ = fi.read()


logger = logging.getLogger(__name__)


class Micrometer():
	"""
	"""
	def __init__(self, ser: "SerialPipe", eol=b"\r\n"):
		self.queue = queue.Queue()
		self.ser = ser
		self.eol = eol

	def __enter__(self):
		self.running = True
		self._last = b""
		self.t = threading.Thread(target=self.run)
		self.t.start()
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		self.running = False
		self.t.join()

	def measure_immediate_ext(self):
		now = time.monotonic()

		while True:
			if not self.running:
				raise RuntimeError("stopped")

			x = self.queue.get()
			if x is None:
				raise RuntimeError("stopped")

			t, v = x

			if t < now:
				continue

			return v["mes"], v


	def measure_immediate(self):
		v, extra = self.measure_immediate_ext()
		return v

	def step(self):
		line = self.ser.read_until(self.eol)
		now = time.monotonic()
		#logger.info("Line: %s", line.hex())

		mes = float(line.strip())

		res = dict(
		 t=now,
		 mes=mes,
		)

		#logger.info("Res: %s", res)

		self.queue.put((now, res))

	def run(self):
		while self.running:
			try:
				self.step()
			except Exception as e:
				logger.exception("Error while stepping: %s", e)

