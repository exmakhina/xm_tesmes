#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Driver for Serial-attached relay

import contextlib
import logging


logger = logging.getLogger(__name__)


class Relay:
	"""
	A relay

	Relays might be multi-channel.
	Each channel is set by passing 0 (=NC) or 1 (=NO)

	Relays might be monostable or bistable, it doesn't matter.
	"""
	def __init__(self, ser):
		self.ser = ser

	def __enter__(self):

		trailer = self.ser.read(100, timeout=0.1)

		if trailer:
			logger.info("Trailer: %s", trailer)

		self.ser.write(b"N")

		self.n = int(self.ser.readline())

		logger.info("#relays %s", self.n)

		return self

	def __exit__(self, *args):
		pass

	def get(self):
		self.ser.write(b"R")

		x = self.ser.readline()

		logger.debug("Line: %s", x)

		res = []
		for c in x[:self.n]:
			res.append(bool(c - ord('0')))

		return res

	def set(self, *value):
		assert len(value) == self.n, f"Wanted {self.n} items but got {value}"

		msg = b"W" + b"".join(str(v).encode() for v in value)
		data = self.ser.write(msg)

	@contextlib.contextmanager
	def in_end_set(self, *value):
		yield
		self.set(*value)
