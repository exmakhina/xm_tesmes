import os
import time
import logging

from exmakhina.serialpipe.serialpipe_command import SerialPipeCommand

from .relay_diy import Relay


logger = logging.getLogger(__name__)


defcmd = "ssh bbg4xm1 socat stdio open:/dev/serial/by-id/usb-Waveshare_RP2040_Zero_E6625C05E74A7325-if00,rawer,b115200"
cmd = os.environ.get("RELAY_STDIO_COMMAND", defcmd)


def test_single(loops=5):
	"""
	Activate a single relay
	"""

	with SerialPipeCommand(cmd) as serialpipe:
		r = Relay(serialpipe)
		with r:

			logger.info("N: %d", r.n)

			status = r.get()
			logger.info("Status: %s", status)

			z = [0] * r.n

			for idx_loop in range(loops):

				for i in range(r.n):

					r.set(*z)

					time.sleep(0.1)

					a = []
					for j in range(r.n):
						if j == i:
							a.append(1)
						else:
							a.append(0)

					logger.info("- %3d/%3d: %s", i+1, r.n, a)

					r.set(*a)
					time.sleep(0.1)

			r.set(*z)
			status = r.get()
			logger.info("Status: %s", status)


def test_combinations_gray():
	"""
	Test all activation combinations fast
	"""

	with SerialPipeCommand(cmd) as serialpipe:
		r = Relay(serialpipe)
		with r:
			logger.info("N: %d", r.n)

			status = r.get()
			logger.info("Status: %s", status)

			z = [0] * r.n
			combinations = 1<<r.n

			for i in range(combinations):
				a = []
				for j in range(r.n):
					if i & (1<<j) != 0:
						a.append(1)
					else:
						a.append(0)

				def b2g(binary):
					binary = tuple(reversed(binary))
					gray = [binary[0]];
					for i in range(1, len(binary)):
						gray.append(binary[i - 1] ^ binary[i])
					return gray

				a = b2g(a)

				logger.info("- %3d/%3d: %s", i+1, combinations, a)

				r.set(*a)
				time.sleep(0.1)

			r.set(*z)
			status = r.get()
			logger.info("Status: %s", status)


def test_combinations_bin():
	"""
	Test all activation combinations
	"""

	with SerialPipeCommand(cmd) as serialpipe:
		r = Relay(serialpipe)
		with r:

			logger.info("N: %d", r.n)

			status = r.get()
			logger.info("Status: %s", status)

			z = [0] * r.n

			combinations = 1<<r.n

			for i in range(combinations):

				r.set(*z)

				time.sleep(0.1)

				a = []
				for j in range(r.n):

					if i & (1<<j) != 0:
						a.append(1)
					else:
						a.append(0)

				logger.info("- %3d/%3d: %s", i+1, combinations, a)

				r.set(*a)
				time.sleep(0.1)

			r.set(*z)
			status = r.get()
			logger.info("Status: %s", status)

def test_masstoggle():
	"""
	Test mass toggling
	"""

	with SerialPipeCommand(cmd) as serialpipe:
		r = Relay(serialpipe)
		with r:

			logger.info("N: %d", r.n)

			status = r.get()
			logger.info("Status: %s", status)

			z = [0] * r.n
			o = [1] * r.n

			combinations = 1<<r.n

			for i in range(10):
				r.set(*z)
				time.sleep(0.1)
				r.set(*o)
				time.sleep(0.1)
