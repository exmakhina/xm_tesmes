class Wavegen:
	def waveform_set(self, vs):
		"""
		Load a waveform into the generator.

		:param vs: voltages
		"""
		raise NotImplementedError()

	def timed_waveform_set(self, ts, vs):
		"""
		Load a waveform into the generator, and set the frequency according to times.

		:param ts: times
		:param vs: voltages
		"""
		raise NotImplementedError()

	def frequency_set(self, value):
		raise NotImplementedError()

	def frequency_get(self) -> float:
		raise NotImplementedError()

