import itertools
import time
import logging
import typing as t
import struct

logger = logging.getLogger(__name__)


class Wavegen:
	def __init__(self, scpi_wg):
		self.scpi = scpi_wg

	def __enter__(self):
		logger.debug(self.scpi.ask("*IDN?"))
		self.enter_remote_mode()
		return self

	def __exit__(self, ext_type, exc_value, exc_traceback):
		pass

	def enter_remote_mode(self):
		self.scpi.write("SYSTEM:REMOTE")

	def _raw_write(self, data):
		for i in range(len(data)):
			c = data[i:i+1]
			self.scpi.ser.write(c)
			time.sleep(0.002)


	def frequency_set(self, value):
		self.scpi.write(f"FREQ {value}")

	def frequency_get(self, value):
		return float(self.scpi.ask("FREQ?"))


	def waveform_set_ascii(self, ampl: t.Sequence[float]):
		"""
		Download sequence of amplitude points (slower)

		:param ampl: list of amplitude points (between -1 and 1)

		"""
		t0 = time.time()
		self._raw_write("DATA VOLATILE, {}\r\n".format(",".join([x.__format__(".3f") for x in ampl])).encode())
		t1 = time.time()
		logger.debug("Sent %d samples in %.3f s", len(ampl), t1-t0)

		self.scpi.write("FUNC:USER VOLATILE")
		self.scpi.write("FUNC:SHAP USER")

	def waveform_set_binary(self, ampl: t.Sequence[float]):
		"""
		Download sequence of amplitude points (faster)

		:param ampl: list of amplitude points (between -1 and 1)
		"""
		t0 = time.time()
		buf = b"".join(struct.pack(">h", round(x*2047)) for x in ampl)
		assert len(buf) == 2 * len(ampl)
		n = len(buf)
		l = len(str(n))
		cmd = f"DATA:DAC VOLATILE, #{l}{n}".encode() + buf + "\r\n".encode()
		#logger.debug("Send %s", cmd)
		self._raw_write(cmd)
		t1 = time.time()
		logger.debug("Sent %d samples in %.3f s", len(ampl), t1-t0)

		self.scpi.write("FUNC:USER VOLATILE")
		self.scpi.write("FUNC:SHAP USER")

	def waveform_set(self, ampl: t.Sequence[float]):
		_min = 100
		_max = -100
		for v in ampl:
			_min = min(v, _min)
			_max = max(v, _max)

		vpp2 = (_max - _min) * 0.5
		_avg = (_max + _min) * 0.5
		logger.debug("VPP2: %f avg: %f", vpp2, _avg)

		ampl_normalized = [ (x - _avg) / (_max - _min) for x in ampl ]

		self.waveform_set_binary(ampl_normalized)
		self.scpi.write(f"VOLT {vpp2}")
		self.scpi.write(f"VOLT:OFFS {_avg}")

	def timed_waveform_set(self,
	 times: t.Sequence[float],
	 ampl: t.Sequence[float],
	 ):
		self.waveform_set(ampl)

		span = times[-1] - times[0]
		dt = times[1] - times[0]
		logger.debug("samples: %s span: %s", len(times), times[-1]-times[0])
		logger.debug("dt: %s", dt)
		freq = 1 / (span + dt)
		self.frequency_set(freq)
