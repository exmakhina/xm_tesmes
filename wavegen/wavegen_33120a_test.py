import os
import contextlib
import numpy as np

from ..scpi.scpi_command import SCPI
from .wavegen_33120a import Wavegen


def create_sample_waveform():
	ts = np.arange(0, 1000) * 1e-4
	amplitude =  (2 * np.abs(ts - 0.5))
	vs = 2 * (amplitude - min(amplitude)) / (max(amplitude) - min(amplitude)) - 1
	vs += np.random.randn(len(ts)) * 0.1
	vs *= -1
	vs[vs>1] = 1
	vs[vs<-1] = -1

	for i, t in enumerate(ts):
		vs[i] = -7 + 3 * (i % 3)

	return ts, vs


def test_arb():
	cmd = os.environ.get("WG_3312A_STDIO_COMMAND",
		"ssh SolidPC socat stdio open:/dev/serial/by-id/usb-1a86_USB2.0-Ser_-if00-port0,rawer,b9600,clocal=1,crtscts=1",
	)
	with contextlib.ExitStack() as stack:
		scpi_wg = SCPI(cmd)
		stack.enter_context(scpi_wg)

		wg = Wavegen(scpi_wg)
		stack.enter_context(wg)

		time, amplitude = create_sample_waveform()
		wg.timed_waveform_set(time, amplitude)
