import logging

from .luxmeter_ut383 import *


logger = logging.getLogger(__name__)


bdaddr = os.environ.get("LUXMETER_UT383BT_BDADDR",
 "E8:D0:3C:51:79:03"
)

def test_basic():

	logging.getLogger("bleak.backends.bluezdbus.manager").setLevel(logging.INFO)
	logging.getLogger("bleak.backends.bluezdbus.client").setLevel(logging.INFO)

	with Luxmeter(bdaddr) as meter:
		while True:
			res = meter.measure_immediate()
			logger.info("got %s", res)
		logger.info("done")
