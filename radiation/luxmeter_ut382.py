#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Luxmeter implementation for the UNI-T UT382 USB luxmeter

import io
import re
import time
import sys
import logging
import queue
import threading


with io.open(__file__.replace(".py", ".rst"), "r") as fi:
	__doc__ = fi.read()


logger = logging.getLogger(__name__)


class Luxmeter():
	"""
	"""
	def __init__(self, ser: "SerialPipe"):
		self.queue = queue.Queue()
		self.ser = ser

	def __enter__(self):
		self.running = True
		self._last = b""
		self.t = threading.Thread(target=self.run)
		self.t.start()
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		self.running = False
		self.t.join()

	def measure_immediate_ext(self):
		now = time.monotonic()

		while True:
			if not self.running:
				raise RuntimeError("stopped")

			x = self.queue.get()
			if x is None:
				raise RuntimeError("stopped")

			t, v = x

			if t < now:
				continue

			return v["lux"], v


	def measure_immediate(self):
		v, extra = self.measure_immediate_ext()
		return v

	def step(self):
		line = self.ser.read_until(b"\r\n")
		now = time.monotonic()
		#logger.info("Line: %s", line.hex())

		#assert line[0] in (0x00,0x27), line
		#logger.info("Line: %s", line[1:].hex())
		assert line[1] == 0x30, line
		assert line[2] == 0x30, line
		logger.debug("Line: %s", line[3:])


		lut = {
		 b";0": float("NaN"), #"L"
		 b"00": 0, # ""
		 b";7": 0,
		 b";?": 0, # 0.
		 b"06": 1,
		 b"0>": 1, # 1.
		 b">5": 2,
		 b">=": 2, # 2.
		 b"<7": 3,
		 b"<?": 3, # 3.
		 b"56": 4,
		 b"5>": 4, # "4."
		 b"=3": 5,
		 b"=;": 5, # "5."
		 b"?3": 6,
		 b"?;": 6, # "6."
		 b"07": 7,
		 b"0?": 7, # "7."?
		 b"?7": 8,
		 b"??": 8, # "8." ?
		 b"=7": 9,
		 b"=?": 9, # "9.",
		 }

		d0 = lut[line[3:5]]
		d1 = lut[line[5:7]]
		d2 = lut[line[7:9]]
		d3 = lut[line[9:11]]
		v = d3* 1000 + d2 * 100 + d1 * 10 + d0

		# TODO better decode to avoid auto-range issues
		# TODO build number from above

		if b"824<" in line:
			v /= 10
		if b"826<" in line:
			v /= 10
		if b"924?" in line:
			v *= 10
		if b"926?" in line:
			v *= 10
		if b"8248" in line:
			v /= 100
		if b"8268" in line:
			v /= 100

		lux = v

		res = dict(
		 t=now,
		 lux=lux,
		)

		#logger.info("Res: %s", res)

		self.queue.put((now, res))

	def run(self):
		while self.running:
			try:
				self.step()
			except Exception as e:
				logger.exception("Error while stepping: %s", e)
