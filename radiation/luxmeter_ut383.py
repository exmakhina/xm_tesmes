#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Luxmeter implementation for the UNI-T UT383 BT luxmeter, using bleak for BLE comm

import io
import re
import time
import sys
import asyncio
import logging
import queue
import threading

import bleak


with io.open(__file__.replace(".py", ".rst"), "r") as fi:
	__doc__ = fi.read()


logger = logging.getLogger(__name__)


class Luxmeter():
	"""
	"""
	def __init__(self, bdaddr):
		self.queue = queue.Queue()
		self.bdaddr = bdaddr

	def __enter__(self):
		self.running = True
		self._last = b""
		self.t = threading.Thread(target=self.run)
		self.t.start()
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		self.running = False
		self.t.join()

	def measure_immediate_ext(self):
		now = time.monotonic()

		while True:
			if not self.running:
				raise RuntimeError("stopped")

			x = self.queue.get()
			if x is None:
				raise RuntimeError("stopped")

			t, v = x

			if t < now:
				continue

			return v["lux"], v


	def measure_immediate(self):
		v, extra = self.measure_immediate_ext()
		return v

	async def _run(self):
		client = bleak.BleakClient(self.bdaddr)
		try:
			while True:
				try:
					await client.connect()
					break
				except bleak.exc.BleakDeviceNotFoundError as e:
					logger.info("Device not found...")
				except bleak.exc.BleakError as e:
					logger.info("Retrying (%s)", e)
				except bleak.exc.BleakDBusError as e:
					logger.info("E: %s %s", e, dir(e))
					if "le-connection-abort-by-local" in str(e):
						continue
					raise

			logger.info("Connected")
			uid = "0000ff02-0000-1000-8000-00805f9b34fb"
			uid_w = "0000ff01-0000-1000-8000-00805f9b34fb"
			await client.start_notify(uid, self.callback)
			while self.running:
				await client.write_gatt_char(uid_w, b"\x5e")
				await asyncio.sleep(0.25)
			await client.stop_notify(uid)
		except Exception as e:
			logger.exception("Exception: %s", e)
		finally:
			self.queue.put(None)
			await client.disconnect()

	def run(self):
		asyncio.run(self._run())

	def callback(self, x, y):
		now = time.monotonic()

		if (m := re.match(rb"^\s*(?P<lux>\d+)LUX;(?P<val>\d+)$", y[5:5+11])) is None:
			logger.warning("Got: %s", y)
			return

		lux = int(m.group("lux"))

		res = dict(
		 t=now,
		 lux=lux,
		)

		logger.info("Res: %s", res)

		self.queue.put((now, res))
