import logging
import contextlib
import os
import time

from ..scpi.scpi_command import SCPI

from .luxmeter_ut382 import *


logger = logging.getLogger(__name__)


cmd = os.environ.get("LUXMETER_UT382_STDIO_COMMAND",
 "socat open:/dev/serial/by-id/usb-Silicon_Labs_CP2104_USB_to_UART_Bridge_Controller_02B98E20-if00-port0,rawer,b19200 stdio"
)

def test_basic():
	with contextlib.ExitStack() as stack:
		scpi = SCPI(cmd, eol=b"\n")
		stack.enter_context(scpi)
		logger.info("pouet")
		ser = scpi.ser

		meter = Luxmeter(ser)
		stack.enter_context(meter)

		while True:
			res = meter.measure_immediate()
			logger.info("got %s", res)
		logger.info("done")
 
