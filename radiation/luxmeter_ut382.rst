#######################
UNI-T UT382 Luminometer
#######################



USB Protocol
############

When USB is enabled (long-press menu, then range+, then exit menu with
menu presses), the device sends CRLF-separated records (lines).


The lines seem to contained an ASCII (except for the first byte)
encoding of raw LCD data.

For the numeric values, we have 8-segment LCDs, which are encoded
with 2 characters.

b"0" is the minimum value, then the 8 segments are encoded with bits:

- First character::

     0
     0
      222
     1
     1
      333

- Second character:

      000
         2
         2

         1
         1
           3


