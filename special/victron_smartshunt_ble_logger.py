#!/usr/env/bin python3
# Data logger for Victron Smart Shunt
# Script will constantly read voltage and current with interval.
import sys
import os
import logging
import argparse
import contextlib
import time

from .victron_smartshunt_ble import Shunt


logger = logging.getLogger(__name__)


def setup_logging(log_level):
	root_log_level = logging.WARNING
	try:
		root_log_level = getattr(logging, log_level)
		loggers = []
	except:
		loggers = []
		for hunk in log_level.split(","):
			k, v = hunk.split(":")
			if k == "*":
				root_log_level = getattr(logging, v)
			else:
				loggers.append((k, getattr(logging, v)))

	try:
		import coloredlogs
		coloredlogs.install(
		 level=root_log_level,
		 logger=logging.root,
		 isatty=args.color,
		)
	except ImportError:
		logging.basicConfig(
		 level=root_log_level,
		 format="%(levelname)s %(name)s %(message)s"
		)

	for name, level in loggers:
		logging.getLogger(name).setLevel(level)


def main(argv=None):
	parser = argparse.ArgumentParser(
	 description="Shunt Logger",
	)

	log_level = os.environ.get("SHUNT_LOG_LEVEL", "INFO")

	parser.add_argument("--color",
	 type=int,
	 choices=[0,1],
	 default=sys.stdin.isatty(),
	)

	parser.add_argument("--log-level",
	 default=log_level,
	 help="Logging level (see Python logging docs); default {}".format(log_level),
	)

	subparsers = parser.add_subparsers(
	 help=f'the command; type "... COMMAND -h" for command-specific help',
	 dest="command",
	)

	subp = subparsers.add_parser(
	 "log",
	 help="Read data and save to csv",
	)

	subp.add_argument("address",
	 help="Shunt BLE address",
	)

	subp.add_argument("file",
	 help="Data dump file path",
	)

	subp.add_argument("--interval",
	 type=float,
	 help="Time between reads",
	 default=0.5,
	)

	subp.add_argument("--flush-interval",
	 type=float,
	 help="Time between flushes",
	 default=None,
	)

	def doit(args):
		with open(args.file, "a") as fo:
			logger.info("Trying to connect")
			with Shunt(args.address) as shunt:
				logger.info("Acquired")
				last_flush = time.time()
				while True:
					t0 = time.time()
					t_v, voltage = shunt.read_voltage()
					t_c, current = shunt.read_current()
					logger.info("V: %.2f, C: %.3f", voltage, current)
					fo.write(f"{t_v:.3f},{voltage:.2f},{current:.3f}\n")
					t1 = now = time.time()
					dt = t0 + args.interval - t1
					if dt > 0:
						time.sleep(dt)
					if args.flush_interval is not None and now > last_flush + args.flush_interval:
						fo.flush()
						last_flush = now

	subp.set_defaults(func=doit)


	with contextlib.suppress(BaseException):
		import argcomplete
		argcomplete.autocomplete(parser)

	args = parser.parse_args(argv)

	setup_logging(args.log_level)

	if getattr(args, "func", None) is None:
		parser.print_help()
		return 1

	return args.func(args)


if __name__ == "__main__":
	res = main()
	raise SystemExit(res)

