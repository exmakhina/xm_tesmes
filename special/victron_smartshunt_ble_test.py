import io
import os
import logging
import time

from .victron_smartshunt_ble import Shunt


logger = logging.getLogger(__name__)


def test_shunt():
	address = os.environ.get("SHUNT_BLE_ADDRESS", "CC:9D:7C:6E:82:DF")
	with Shunt(address) as shunt:
		while True:
			t0 = time.time()
			t1, voltage = shunt.read_voltage()
			logger.info("Voltage: %.2f V after %.3f ms", voltage, (t1-t0) * 1e3)
			with io.open("/tmp/voltage", "a") as fo:
				fo.write(f"{t0}\t{voltage:.2f}\n")
			t0 = time.time()
			t1, current = shunt.read_current()
			logger.info("Current: %.3f A after %.3f ms", current, (t1-t0) * 1e3)
			with io.open("/tmp/current", "a") as fo:
				fo.write(f"{t0}\t{current:.3f}\n")
