##################
Victron SmartShunt
##################

This device is an integrated voltage and current sensor, using a
shunt.

We're using the BLE interface in our driver for it.


Notes:

- Enabling of the characteristics we're using in this driver must be
  done; using the VictronConnect mobile app, one needs to first set
  battery capacity, then go to settings, product info, and enable
  experimental BLE service.

- The default bonding code is 000000

- The driver expects the device to already be bonded, it doesn't
  handle the bonding.

- SmartShunt draws power from what it measures ; it has ~ 30 mW power
  consumption.

- The values seem to be updated every second, so there is no point in
  performing readings more than two times per second.

- Due to large shunt and low price, don't expect to have an
  extraordinary low current measurement accuracy:

  - A test at 5.000 A gives ~3 mA standard deviation, so measurements are
    about +/- 0.25%.

  - A test at 0.100 A gives similar standard deviation, here
    measurements are +/- 10%.

  This noise would be mainly caused by analog-digital conversion
  limitations:

  - The 500A model has a ~ 0.1 mOhm shunt; 50 mV for 500A,
    100 µV for 1 A, 10 µV for 0.1A.

    The telemetry has 0.1 µV resolution and we see 0.3 µV standard
    deviation, so +/- 1 µV accuracy, which is not bad!

- Don't expect to have an extraordinary current measurement accuracy:

  - Zero calibration is available.

  - There's a threshold for snapping low values to zero, defaulting to
    100 mA (and it should be disabled if using the device for measurements).

  - The resistance of the shunt can't be directly measured, because
    the MCU is always connected on it.
    Instead, one must have a known current and measure voltage between
    sense pins.

  - There's no documented means to have gain calibration on the
    device, so in order to do reasonably accurate measurements,
    external calibration should be done.

    The simplest way is to take the readings at known current and
    compensate.


References:

- https://community.victronenergy.com/questions/93919/victron-bluetooth-ble-protocol-publication.html
- https://www.victronenergy.com/battery-monitors/smart-battery-shunt#datasheets
- https://www.victronenergy.com/live/open_source:start

