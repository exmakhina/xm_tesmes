#!/usr/env/bin python3
# Interface for communication with Victron Smart Shunt over BLE

import os
import typing as t
import struct
import asyncio
import logging
import queue
import threading
import time

from bleak import (
 BleakClient,
 BleakScanner,
)


logger = logging.getLogger(__name__)


class Shunt:
	def __init__(self, address: str=None):
		self.address = address
		self.q_req = queue.Queue()
		self.q_res = queue.Queue()

	def __enter__(self):
		self.running = True
		self.t = threading.Thread(target=self.run)
		self.t.start()
		return self

	async def _run(self):
		#device = await BleakScanner.find_device_by_address(self.address)
		#logger.info("Device: %s", device)
		device = self.address
		async with BleakClient(device) as client:
			uid = "6597ffff-4bda-4c1e-af4b-551c4cf74769"
			payload = struct.pack("H", 0xFFFF)
			await client.write_gatt_char(uid, payload, response=True)
			while self.running:
				try:
					uid = self.q_req.get_nowait()
					await asyncio.sleep(0.1)
				except queue.Empty:
					continue
				except Exception as e:
					logger.exception("Oops %s/%s", type(e), e)
					break
				res = await client.read_gatt_char(uid)
				#logger.debug("Res > %s", res)
				now = time.time()
				self.q_res.put((now, res))

	def run(self):
		try:
			asyncio.run(self._run())
		except Exception as e:
			logger.exception("Oops %s/%s", type(e), e)

	def __exit__(self, exc_type, exc_value, exc_traceback):
		logger.debug("Exit: %s/%s %s", exc_type, exc_value, exc_traceback)
		self.running = False
		self.t.join()

	def get_res(self, timeout=25):
		try:
			return self.q_res.get(timeout=timeout)
		except queue.Empty as e:
			raise TimeoutError("Something happend... Check logs") from e

	def read_voltage(self) -> t.Optional[t.Tuple[float,float]]:
		uid = "6597ed8d-4bda-4c1e-af4b-551c4cf74769"
		self.q_req.put(uid)
		now, res = self.get_res()
		val =  struct.unpack("h", res)[0]
		if hex(val) == 0x7FFF:
			logger.info("Voltage not available")
			return
		return now, val * 1e-2

	def read_current(self) -> t.Optional[t.Tuple[float,float]]:
		uid = "6597ed8c-4bda-4c1e-af4b-551c4cf74769"
		self.q_req.put(uid)
		now, res = self.get_res()
		val = struct.unpack("i", res)[0]
		if hex(val) == 0x7FFFFFFF:
			logger.info("Current not available")
			return
		return now, val * 1e-3

	def read_power(self) -> t.Optional[t.Tuple[float,float]]:
		uid = "6597ed8e-4bda-4c1e-af4b-551c4cf74769"
		self.q_req.put(uid)
		now, res = self.get_res()
		val = struct.unpack("h", res)[0]
		if hex(val) == 0x7FFF:
			logger.info("Power not available")
			return
		return now, val

	def read_state_of_charge(self) -> t.Optional[t.Tuple[float,float]]:
		uid = "65970fff-4bda-4c1e-af4b-551c4cf74769"
		self.q_req.put(uid)
		now, res = self.get_res()
		val = struct.unpack("H", res)[0]
		if hex(val) == 0xFFFF:
			logger.info("State of charge not available")
			return
		return now, val

	def read_consumed_Ah(self) -> t.Optional[t.Tuple[float,float]]:
		uid = "6597eeff-4bda-4c1e-af4b-551c4cf74769"
		self.q_req.put(uid)
		now, res = self.get_res()
		val = struct.unpack("i", res)[0]
		return now, val
