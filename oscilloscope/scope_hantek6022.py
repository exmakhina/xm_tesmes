#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Interface for Hantek 6022 oscilloscope
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-tesmes@zougloub.eu> & contributors
# SPDX-License-Identifier: MIT

import sys
import time
import threading
import queue
import contextlib
from collections import deque
import logging

import numpy as np

from PyHT6022.LibUsbScope import Oscilloscope as LibUsbOscilloscope


logger = logging.getLogger(__name__)


class Oscilloscope:
	"""
	Quick & dirty interface for Hantek 6022 oscilloscope

	Based on PyHT6022
	"""
	def __init__(self, timefunc=None, voltagerange=10, samplerate=24e6, numchannels=1, alternative=1):
		if timefunc is None:
			timefunc = time.time

		self.timefunc = timefunc
		if voltagerange > 2.5:
			self.voltagerange = 5
		elif voltagerange > 1:
			self.voltagerange = 2.5
		elif voltagerange > 0.5:
			self.voltagerange = 1
		else:
			self.voltagerange = 0.5

		if samplerate > 1e6:
			self.samplerate = int(round(samplerate/1e6))
		else:
			self.samplerate = 100 + int(round(samplerate / 1e4))

		self.numchannels = numchannels
		self.alternative = alternative  # Interface mode
		self.scope = LibUsbOscilloscope()
		self.frequency = samplerate
		self.blocksize = 16 * 2 * 6 * 1024  # Should be divisible by 6*1024
		self.outstanding_transfers = 20
		self.shutdown_event = threading.Event()
		self.data_queue = queue.Queue()

	def __enter__(self):
		self.scope.setup()
		if not self.scope.open_handle():
			sys.exit("Failed to open scope handle.")
		if not self.scope.is_device_firmware_present:
			self.scope.flash_firmware()
		else:
			self.scope.supports_single_channel = True
		self.scope.set_interface(self.alternative)

		self.scope.set_sample_rate(self.samplerate)

		self.scope.set_num_channels(self.numchannels)

		self.scope.set_ch1_ac_dc( self.scope.DC )
		self.scope.set_ch2_ac_dc( self.scope.DC )

		calibration = self.scope.get_calibration_values()

		vlut = {
		 5: 1,
		 2.5: 2,
		 1: 5,
		 0.5: 10,
		}

		self.scope.set_ch1_voltage_range(vlut[self.voltagerange])
		# TODO
		self.scope.set_ch2_voltage_range(vlut[self.voltagerange])

		return self

	def __exit__(self, exc_type, exc_value, traceback):
		self.scope.close_handle()

	@contextlib.contextmanager
	def streaming(self, raw=False):
		self.shutdown_event.clear()
		self.data_queue = queue.Queue()
		read_async_shutdown_event = threading.Event()

		def extend_callback(ch1_data, ch2_data):
			time_of_reception = self.timefunc()
			time_start = time_of_reception - len(ch1_data) / self.frequency
			self.data_queue.put((time_start, time_of_reception, ch1_data, ch2_data))

		def stream_data():
			self.scope.start_capture()
			read_async_shutdown_event = self.scope.read_async(
				extend_callback,
				self.blocksize,
				outstanding_transfers=self.outstanding_transfers,
				raw=True)
			while not self.shutdown_event.is_set():
				self.scope.poll()
			# Signal to read_async to stop
			read_async_shutdown_event.set()
			time.sleep(1)
			self.scope.stop_capture()

		self.streaming_thread = threading.Thread(target=stream_data)
		self.streaming_thread.start()

		try:
			yield self._data_iterator(raw=raw)
		finally:
			self.shutdown_event.set()
			self.streaming_thread.join()
			self.scope.stop_capture()

	def _data_iterator(self, raw):
		while not self.shutdown_event.is_set() or not self.data_queue.empty():
			try:
				t0, t1, ch1_data, ch2_data = self.data_queue.get(timeout=0.1)
				if raw:
					ch1_data = np.frombuffer(ch1_data, dtype=np.uint8)
					if self.numchannels > 1:
						ch2_data = np.frombuffer(ch2_data, dtype=np.uint8)
					else:
						ch2_data = np.array([], dtype=np.uint8)
					yield t0, t1, ch1_data, ch2_data
				else:
					data = np.frombuffer(ch1_data, dtype=np.uint8)
					volts1 = self._data_to_volts(data)
					if self.numchannels > 1:
						data = np.frombuffer(ch2_data, dtype=np.uint8)
						volts2 = self._data_to_volts(data)
					else:
						volts2 = []
					yield t0, t1, volts1, volts2
			except queue.Empty:
				continue

	def _data_to_volts(self, data):
		volts = (data.astype(np.float32) - 128) / 128 * self.voltagerange
		return volts

	def _find_trigger(self, volts, level, edge):
		level = (level / self.voltagerange) * 128 + 128

		# Returns the index in volts where the trigger occurs, or None if not found
		if edge > 0:
			# Rising edge
			crossings = np.where((volts[:-1] < level) & (volts[1:] >= level))[0]
		else:
			# Falling edge
			sel = (volts[:-1] > level)
			sel &= (volts[1:] <= level)
			crossings = np.where(sel)[0]
		#logger.info("Trigger: %s", crossings.size)
		if crossings.size > 0:
			return crossings[0] + 1  # +1 because we are looking at volts[1:]
		else:
			return None


	@contextlib.contextmanager
	def triggered(self, channel, level, edge, time_before, time_after):
		"""
		A context manager that enables streaming in a thread,
		detects trigger events, and yields data chunks around those events.

		:param channel: channel to trigger on
		Parameters:
		- level: Voltage level to trigger on
		- edge: >0 for rising edge, <0 for falling edge
		- time_before: Time in seconds to include before the trigger event
		- time_after: Time in seconds to include after the trigger event
		"""
		with self.streaming(raw=True) as data_iter:
			max_samples_before = int(time_before * self.frequency)
			max_samples_after = int(time_after * self.frequency)
			buffer1 = deque(maxlen=max_samples_before)
			buffer2 = deque(maxlen=max_samples_before)
			shutdown_event = self.shutdown_event

			def trigger_iterator():
				nonlocal buffer1
				nonlocal buffer2
				idx_chunk = 0
				while not shutdown_event.is_set():
					try:
						time_start, time_end, volts1, volts2 = next(data_iter)
						idx_chunk += 1
					except StopIteration:
						break

					volts = volts1 if channel == 0 else volts2

					# Check for trigger in new data
					trigger_index = self._find_trigger(volts, level, edge)
					if trigger_index is None:
						buffer1.extend(volts1)
						buffer2.extend(volts2)
					else:
						logger.info("Trigger found in chunk %d", idx_chunk)
						if idx_chunk == 1:
							buffer1.clear()
							buffer2.clear()
							logger.info("ignored")
							continue

						trigger_time = time_start + trigger_index / self.frequency

						if trigger_index > max_samples_before:
							pre_trigger_data1 = volts1[trigger_index-max_samples_before:trigger_index]
							if self.numchannels > 1:
								pre_trigger_data2 = volts2[trigger_index-max_samples_before:trigger_index]
						else:
							pre_trigger_data1 = (
							 np.array(list(buffer1)[-(max_samples_before-trigger_index):] +
							 list(volts1[:trigger_index]))
							)
							if self.numchannels > 1:
								pre_trigger_data2 = (
								 np.array(list(buffer2)[-(max_samples_before-trigger_index):] +
								 list(volts2[:trigger_index]))
								)

						post_trigger_data1 = volts1[trigger_index:]
						if self.numchannels > 1:
							post_trigger_data2 = volts2[trigger_index:]

						# Need to collect additional data to cover time_after
						total_samples_have = len(pre_trigger_data1) + len(post_trigger_data1)
						total_samples_want = max_samples_before + max_samples_after

						while total_samples_have < total_samples_want and not shutdown_event.is_set():
							logger.debug("Acquire %d / %d", total_samples_have, total_samples_want)
							try:
								time_start, time_end, volts1, volts2 = next(data_iter)
								post_trigger_data1 = np.concatenate((post_trigger_data1, volts1))
								if self.numchannels > 1:
									post_trigger_data2 = np.concatenate((post_trigger_data2, volts2))
								total_samples_have = len(pre_trigger_data1) + len(post_trigger_data1)
							except StopIteration:
								break

						total_post_trigger_samples = total_samples_want - len(pre_trigger_data1)
						post_trigger_data1 = post_trigger_data1[:total_post_trigger_samples]
						triggered_data1 = np.concatenate((pre_trigger_data1, post_trigger_data1))
						if self.numchannels > 1:
							post_trigger_data2 = post_trigger_data2[:total_post_trigger_samples]
							triggered_data2 = np.concatenate((pre_trigger_data2, post_trigger_data2))
						else:
							triggered_data2 = []

						yield (
						 trigger_time,
						 self._data_to_volts(triggered_data1),
						 self._data_to_volts(triggered_data2),
						)

						buffer1.clear()
						buffer2.clear()

			yield trigger_iterator()
