import logging
import datetime

import numpy as np

from .scope_hantek6022 import Oscilloscope


logger = logging.getLogger(__name__)


def test_streaming():
	data = []

	with Oscilloscope(voltagerange=5) as scope:
		with scope.streaming() as gen:
			for i in range(2):
				t0, t1, chunk, _ = next(gen)
				data.append(chunk)

	l = sum(len(_) for _ in data)

	logger.info("Got %d samples ", l)

	v_v = []
	for chunk in data:
		v_v += list(chunk)

	v_t = np.arange(0, l) / scope.frequency

	import matplotlib.figure

	logging.getLogger("matplotlib.font_manager").setLevel(logging.WARNING)

	fig = matplotlib.figure.Figure(figsize=(12,9))
	ax = fig.add_subplot(1, 1, 1)

	ax.plot(v_t, v_v,
	 ".",
	)

	title = f"Scope"

	ax.grid(True)
	ax.legend()
	ax.set_xlabel("Time (s)")
	ax.set_ylabel("Value (V)")
	fig.suptitle(title)
	fig.savefig(f"scope_hantek6022-streaming.png")

def test_trigger():
	"""
	"""

	with Oscilloscope(voltagerange=5, numchannels=2, samplerate=16e6) as scope:
		scope.scope.set_calibration_frequency(1e3)
		with scope.triggered(0, 0.7, -1, 0.5e-3, 1.5e-3) as gen:
			t_trig, data1, data2 = next(gen)
			ts_trig = datetime.datetime.fromtimestamp(t_trig)
			logger.info("Trigger at %s: %s", ts_trig, data1)

	l = len(data1)

	logger.info("Got %d samples ", l)

	v_t = 1e3 * np.arange(0, l) / scope.frequency

	import matplotlib.figure

	logging.getLogger("matplotlib.font_manager").setLevel(logging.WARNING)

	fig = matplotlib.figure.Figure(figsize=(12,6))
	ax = fig.add_subplot(1, 1, 1)


	color = 'tab:blue'
	ax.set_xlabel("Time (ms)")
	ax.set_ylabel("Voltage (V)", color=color)
	ax.tick_params(axis="y", labelcolor=color)
	ax.plot(v_t, data1,
	 #".",
	 color=color,
	 label="RI",
	)

	ax2 = ax.twinx()
	color = 'tab:red'
	ax2.set_ylabel("Voltage (V)", color=color)
	ax2.plot(v_t, data2 * 10,
	 #".",
	 color=color,
	 label="Rx",
	)
	ax2.tick_params(axis="y", labelcolor=color)

	title = f"Trigger test"

	ax.grid(True)
	#ax.legend()
	#ax2.legend()
	fig.suptitle(title)
	fig.savefig(f"scope_hantek6022-trigger.png")
