#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Telemeter implementation - GLM400c and maybe others

import io
import struct
import time
import asyncio
import logging
import queue
import threading

import bleak


with io.open(__file__.replace(".py", ".rst"), "r") as fi:
	__doc__ = fi.read()


logger = logging.getLogger(__name__)


class Telemeter():
	"""
	"""
	def __init__(self, bdaddr):
		self.queue = queue.Queue()
		self.bdaddr = bdaddr

	def __enter__(self):
		self.running = True
		self._last = b""
		self.t = threading.Thread(target=self.run)
		self.t.start()
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		self.running = False
		self.t.join()

	def measure_immediate_ext(self):
		now = time.monotonic()

		while True:
			if not self.running:
				raise RuntimeError("stopped")

			x = self.queue.get()
			if x is None:
				raise RuntimeError("stopped")

			t, v = x

			if t < now:
				continue

			return v["d"], v


	def measure_immediate(self):
		v, extra = self.measure_immediate_ext()
		return v

	async def _run(self):
		client = bleak.BleakClient(self.bdaddr)
		try:
			while True:
				try:
					await client.connect()
					break
				except bleak.exc.BleakDeviceNotFoundError as e:
					logger.info("Device not found...")
				except bleak.exc.BleakError as e:
					logger.info("Retrying (%s)", e)
				except bleak.exc.BleakDBusError as e:
					logger.info("E: %s %s", e, dir(e))
					if "le-connection-abort-by-local" in str(e):
						continue
					raise

			logger.info("Connected")
			uid = "02a6c0d1-0451-4000-b000-fb3210111989"
			await client.start_notify(uid, self.callback)

			await asyncio.sleep(0.5)
			# switch on/off?
			while self.running:
				logger.debug("ping!")
				# Turn on
				await client.write_gatt_char(uid, bytes.fromhex("c0550201001a"), response=True)
				x = await client.write_gatt_char(uid, bytes.fromhex("c05601001e"), response=True)
				await asyncio.sleep(0.25)
			await client.stop_notify(uid)
		except Exception as e:
			logger.exception("Exception: %s", e)
		finally:
			self.queue.put(None)
			await client.disconnect()

	def run(self):
		asyncio.run(self._run())

	def callback(self, x, y):
		now = time.monotonic()

		# TODO we could validate CRC8, but we're using bluetooth...

		if not y.startswith(b"\xc0\x55\x10"):
			logger.debug("skip non-data %s", y)
			return
		else:
			d, = struct.unpack("<f", y[7:11])

		if d == 0.0:
			logger.debug("skipping zero: %s", y)
			return

		logger.debug("Pong! %s", y)

		res = dict(
		 t=now,
		 d=d,
		)

		logger.debug("Res: %s", res)

		self.queue.put((now, res))
