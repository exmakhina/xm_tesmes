import logging
import os

from .telemeter_glm import *


logger = logging.getLogger(__name__)


bdaddr = os.environ.get("TELEMETER_GLM_BDADDR",
 "0C:EC:80:7F:22:78"
)

def test_basic():

	if 1:
		logging.getLogger("bleak.backends.bluezdbus.manager").setLevel(logging.INFO)
		logging.getLogger("bleak.backends.bluezdbus.client").setLevel(logging.INFO)

	for i in range(10):
		with Telemeter(bdaddr) as meter:
			for i in range(10):
				res = meter.measure_immediate()
				logger.info("got %s", res)
			logger.info("done")
