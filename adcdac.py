#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# ADC/DAC conversion code
# SPDX-FileCopyrightText: 2008,2025 Jérôme Carretero <cJ-tesmes@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging

import numpy as np


logger = logging.getLogger(__name__)


class ADC:
	"""
	Note on convention:
	- LSB is difference of voltage references divided by 2**nob
	- 0 represents negative voltage reference
	- 2**nob-1 represents the positive reference voltage minus one LSB
	- LSB can also be considered as full-scale range divided by 2**nob - 1
	"""
	def __init__(self,
	 nob: int,
	 vmax: float,
	 vmin: float=0,
	 tp=np.int32,
	):
		"""
		Initialize the ADC
		"""
		self.nob = nob
		self.vmax = vmax
		self.vmin = vmin
		self.steps = 2**nob
		self.lsb = (vmax - vmin) / self.steps
		self.full_scale = vmax - vmin - self.lsb
		self.tp = tp
		self._1_lsb = 1/self.lsb

	def __call__(self, v: float) -> int:
		"""
		:param v: analog input value
		:return: corresponding digital value
		"""
		return np.array(
		 np.round(
		  (np.clip(v, self.vmin, self.full_scale) - self.vmin)
		  * self._1_lsb),
		 dtype=self.tp)


class DAC:
	"""
	Note: see ADC note on convention.
	"""
	def __init__(self,
	 nob: int,
	 vmax: float,
	 vmin: float=0,
	 tp=np.float32,
	):
		"""
		Initialize the DAC
		"""
		self.nob = nob
		self.vmax = vmax
		self.vmin = vmin
		self.steps = 2**nob
		self.lsb = (vmax - vmin) / self.steps
		self.full_scale = vmax - vmin - self.lsb
		self.tp = tp
		self._1_lsb = 1/self.lsb

	def __call__(self, i: int) -> float:
		"""
		:param v: digital input value
		:return: corresponding analog value
		"""
		return (
		 np.array(
		  np.clip(i, 0, self.steps-1),
		  dtype=self.tp)
		 * self.lsb + self.vmin)
