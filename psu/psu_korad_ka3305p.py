#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Userspace driver for the Korad KA3305P programmable power supply

import logging
import sys, time
import contextlib


logger = logging.getLogger(__name__)


safe_time_delta = 0.1


class PSU(object):
	def __init__(self, scpi: "SCPI"):
		"""
		:param SCPI: SCPI adapter, configured for rx EOL=b"\x00" and tx EOL=b""
		"""
		self.scpi = scpi
		self._last_cmd = time.monotonic()

	def __enter__(self):
		logger.info("PSU Enter")
		idn = self.idn()
		logger.info("IDN: %s", idn)
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		logger.info("PSU Exit")
		pass

	def idn(self):
		return self.scpi.ask("*IDN?")

	def voltage_setpoint_get(self, output=0):
		return self._getvalue(f"VSET{output+1}?")

	def voltage_setpoint_set(self, value, output=0):
		self._setvalue(f"VSET{output+1}", value)

	def current_setpoint_get(self, output=0):
		return self._getvalue(f"ISET{output+1}?")

	def current_setpoint_set(self, value, output=0):
		self._setvalue(f"ISET{output+1}", value)

	def sense_voltage(self, output=0):
		"""
		May return 0 as a bug
		"""
		return self._getvalue(f"VOUT{output+1}?")

	def sense_current(self, output=0):
		"""
		May return 0 as a bug
		"""
		return self._getvalue(f"IOUT{output+1}?")

	def _getvalue(self, word):
		"""
		Retrieve a value from the PSU

		:param word: the word to read (eg. "ISET1?")

		Note: currently {I,V}{SET,OUT}{1,2} all use l=5.
		"""

		now = time.monotonic()
		if now < self._last_cmd + safe_time_delta:
			time.sleep(self._last_cmd + safe_time_delta - now)
		self._last_cmd = time.monotonic()

		while True:
			self.scpi.write(word)
			data = self.scpi.read(5)
			if len(data) != 5:
				logger.warning("Trying again to read (got %s)", data)
				continue
			break

		return float(data)

	def _setvalue(self, word, value):
		"""
		Set a value

		:param word: the word to set (eg. "ISET1")
		:param value: the value to set it to
		"""
		fmt = {
		 "ISET1": "%5.3f",
		 "ISET2": "%5.3f",
		 "VSET1": "%5.2f",
		 "VSET2": "%5.2f",
		}[word]
		now = time.monotonic()
		if now < self._last_cmd + safe_time_delta:
			time.sleep(self._last_cmd + safe_time_delta - now)
		self.scpi.write("%s:%s" % (word, fmt % value))
		self._last_cmd = time.monotonic()

	def set_on(self, doit=True):
		now = time.monotonic()
		if now < self._last_cmd + safe_time_delta:
			time.sleep(self._last_cmd + safe_time_delta - now)
		self.scpi.write("OUT%d" % doit)
		self._last_cmd = time.monotonic()

	enable = set_on

	def enabled(self):
		raise NotImplementedError()

	@contextlib.contextmanager
	def in_enabled(self):
		logger.info("PSU Enabled")
		self.enable()
		try:
			yield self
		finally:
			logger.info("PSU Disable")
			#breakpoint()
			self.enable(False)
			logger.info("PSU Disabled")

	def lock(self, doit=True):
		now = time.monotonic()
		if now < self._last_cmd + safe_time_delta:
			time.sleep(self._last_cmd + safe_time_delta - now)
		self.scpi.write("LOCK%d" % doit, l=None)
		self._last_cmd = time.monotonic()
