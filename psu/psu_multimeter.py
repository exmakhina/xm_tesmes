#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# PSU ABC

import logging
import typing as t


logger = logging.getLogger(__name__)


class PSU:
	"""
	PSU monitor
	"""
	def __init__(self,
		 voltage_sensors: t.Optional[t.Mapping[int,callable]]=None,
		 current_sensors: t.Optional[t.Mapping[int,callable]]=None,
		 ):
		self.voltage_sensors = voltage_sensors
		self.current_sensors = current_sensors

	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		pass

	def voltage_setpoint_get(self, output=0):
		raise NotImplementedError("Multimeter PSU can't do this")

	def voltage_setpoint_set(self, value, output=0):
		raise NotImplementedError("Multimeter PSU can't do this")

	def current_setpoint_get(self, output=0):
		raise NotImplementedError("Multimeter PSU can't do this")

	def current_setpoint_set(self, value, output=0):
		raise NotImplementedError("Multimeter PSU can't do this")

	def sense_voltage(self, output=0):
		return self.voltage_sensors.get(output, lambda: float("NaN"))()

	def sense_current(self, output=0):
		return self.current_sensors.get(output, lambda: float("NaN"))()
