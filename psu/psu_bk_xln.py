#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# PSU implementation for B&K Precision XLN Series
# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging
import time
import contextlib
import typing as t

from ..scpi.scpi_instrument import scpi_common

logger = logging.getLogger(__name__)


@scpi_common(star_cls=..., syst_err="SYSTEM:ERROR?")
class PSU:
	"""
	"""
	def __init__(self, scpi: "SCPI"):
		"""
		:param scpi: probably a ..scpi.serial.SCPI object
		"""
		self.s = self.scpi = scpi

	def __enter__(self):
		idn = self.s.ask("*IDN?")
		logger.info("IDN: %s", idn)
		assert idn.startswith("B&K PRECISION,XLN"), f"Unknown PSU {idn}!"
		self.model = idn.split(",")[1]
		if self.model == "XLN8018":
			self.v_range = (0, 80)
			self.i_range = (0.01, 18)
		else:
			self.v_range = None
			self.i_range = None
		return self

	def __exit__(self, ext_type, exc_value, exc_traceback):
		pass

	def enable(self, doit=True):
		self.s.write("OUT {}".format("ON" if doit else "OFF"))

	def voltage_setpoint_get(self):
		return float(self.s.ask(":VOLT?"))

	def voltage_setpoint_set(self, value):
		if self.v_range:
			lo, hi = self.v_range
			if value < lo:
				raise RuntimeError(f"Value {value} for voltage is too low (min {lo})")
			if value > hi:
				raise RuntimeError(f"Value {value} for voltage is too high (min {hi})")
		self.s.write(f":VOLT {value}")

	def current_setpoint_get(self):
		return float(self.s.ask(f":CURR?"))

	def current_setpoint_set(self, value):
		if self.i_range:
			lo, hi = self.i_range
			if value < lo:
				raise RuntimeError(f"Value {value} for current is too low (min {lo})")
			if value > hi:
				raise RuntimeError(f"Value {value} for current is too high (min {hi})")
		self.s.write(f":CURR {value}")

	def sense_voltage(self):
		return float(self.s.ask("MEAS:VOLT?"))

	def sense_current(self):
		return float(self.s.ask("MEAS:CURR?"))

	def configure_list(self, t_v_i: t.Sequence[t.Tuple[float,float,float]],
	 prog_idx: int=1, repeat: int=0, prog_after: int=0):
		self.s.write(f"PROG {prog_idx}")
		self.s.write(f"PROG:CLE")
		self.s.write(f"PROG:REP {repeat}")
		self.s.write(f"PROG:TOTA {len(t_v_i)}")
		for idx_step, (t, v, i) in enumerate(t_v_i):
			self.s.write(f"PROG:STEP {idx_step+1}")
			self.s.write(f"PROG:STEP:CURR {i}")
			self.s.write(f"PROG:STEP:VOLT {v}")
			self.s.write(f"PROG:STEP:ONT {t}")
		self.s.write(f"PROG:NEXT {prog_after}")
		self.s.write(f"PROG:SAV")

	def run_list(self, prog_idx: int=1):
		self.s.write(f"PROG {prog_idx}")
		self.s.write(f"PROG:RUN ON")

	def enabled(self):
		return self.s.ask("OUTPUT:STATE?") != "OFF"

	@contextlib.contextmanager
	def in_enabled(self):
		old = self.enabled()
		logger.info("Enter PSU Enabled (current state %s)", old)
		self.enable()
		try:
			yield self
		finally:
			logger.info("Exit PSU Enabled (restoring %s)", old)
			self.enable(old)

