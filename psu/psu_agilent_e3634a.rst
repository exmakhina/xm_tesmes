##################
Agilent E3634A PSU
##################

References:

- https://www.keysight.com/us/en/product/E3634A/200w-power-supply-25v-7a-50v-4a.html
- https://www.keysight.com/us/en/assets/7018-06785/data-sheets/5968-9726.pdf
- https://www.keysight.com/us/en/assets/9018-01164/user-manuals/9018-01164.pdf


