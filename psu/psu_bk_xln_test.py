import logging
import os
import time
import signal
import itertools

from ..scpi.scpi_command import SCPI
from .psu_bk_xln import PSU


logger = logging.getLogger(__name__)


cmd = os.environ.get("BK_XLN_STDIO_COMMAND",
 "ssh SolidPC socat stdio open:/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_277K12122-if00-port0,rawer,b57600",
)

def test_enter():
	with SCPI(cmd, eol_rx=b"\r\n", eol_tx=b"\r\n") as scpi:
		with PSU(scpi) as psu:
			pass

def test_enable():
	with SCPI(cmd, eol_rx=b"\r\n", eol_tx=b"\r\n") as scpi:
		with PSU(scpi) as psu:
			for i in range(5):
				psu.enable(True)
				psu.enable(False)

def test_setpoint_v():
	with SCPI(cmd, eol_rx=b"\r\n", eol_tx=b"\r\n") as scpi:
		with PSU(scpi) as psu:
			for sp in itertools.chain([1], range(1000, 80000+1000, 1000)):
				v_ref = round(sp * 1e-3, 3)
				psu.voltage_setpoint_set(v_ref)
				v_mes = psu.voltage_setpoint_get()
				logger.info("%f %f", v_ref, v_mes)
				assert v_ref == v_mes

def test_setpoint_i():
	with SCPI(cmd, eol_rx=b"\r\n", eol_tx=b"\r\n") as scpi:
		with PSU(scpi) as psu:
			for sp in itertools.chain([1], range(100, 1800+1, 100)):
				i_ref = round(sp * 1e-2, 2)
				psu.current_setpoint_set(i_ref)
				i_mes = psu.current_setpoint_get()
				logger.info("%f %f", i_ref, i_mes)
				assert i_ref == i_mes

def test_in_enabled():
	for i in range(10):
		try:
			with SCPI(cmd, eol_rx=b"\r\n", eol_tx=b"\r\n") as scpi:
				with PSU(scpi) as psu:
					psu.voltage_setpoint_set(7.3)
					psu.current_setpoint_set(0.01)
					with psu.in_enabled():
						time.sleep(1)
						os.kill(os.getpid(), signal.SIGINT)
						#1/0
		except ZeroDivisionError:
			pass
		except KeyboardInterrupt:
			pass
