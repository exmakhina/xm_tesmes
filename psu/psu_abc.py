#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# PSU ABC

import logging


logger = logging.getLogger(__name__)


class PSU:
	def __init__(self):
		pass

	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		pass

	def voltage_setpoint_get(self, output=0):
		return float("NaN")

	def voltage_setpoint_set(self, value, output=0):
		return float("NaN")

	def current_setpoint_get(self, output=0):
		return float("NaN")

	def current_setpoint_set(self, value, output=0):
		return float("NaN")

	def sense_voltage(self, output=0):
		return float("NaN")

	def sense_current(self, output=0):
		return float("NaN")
