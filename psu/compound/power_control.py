#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# PSU wrapper to provide constant power supply
# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ@zougloub.eu> & contributors
# SPDX-License-Identifier: MIT

import logging
import threading
import time


logger = logging.getLogger(__name__)


class PSU:
	def __init__(self, parent):
		self.parent = parent
		self._dt = 0.1

	def __enter__(self):
		self.running = True
		self.step()
		self.t = threading.Thread(target=self.run)
		self.t.start()
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		self.running = False
		self.t.join()

	def power_setpoint_get(self):
		return self.power

	def power_setpoint_set(self, value):
		self.power = value

	def sense_power(self):
		return self.i_mes * self.v_mes

	def run(self):
		"""
		"""

		while self.running:
			try:
				self.step()
				time.sleep(self._dt)
			except KeyboardInterrupt:
				logger.info("Keyboard interrupt")
				break
			except Exception as e:
				logger.exception("Unexpected error: %s", e)
				break

	def step(self):
		"""
		"""
		psu = self.psu

		p_ref = self.power

		self.v_set = v_set = psu.voltage_setpoint_get()
		self.i_set = i_set = psu.current_setpoint_get()
		self.v_mes = v_mes = psu.sense_voltage()
		self.i_mes = i_mes = psu.sense_current()

		logger.debug("V = %f (%f)", v_mes, v_set)
		logger.debug("I = %f (%f)", i_mes, i_set)

		p_set = v_set * i_set
		p_mes = v_mes * i_mes

		logger.debug("I(%f) = %f", v_mes, i_ref)

		logger.debug("I = %f", i_mes)

		p_err = p_ref - p_mes
		logger.debug("Error: %f", i_err)

		cv = abs(v_set-v_mes) < 0.01
		if cv:
			Kp = 5
			v_new = min(v_set + Kp * p_err, self._ocv)
			logger.debug("Command V=%f", v_new)
			psu.voltage_setpoint_set(v_new)
		else:
			Kp = 0.5
			i_new = min(i_set + Kp * p_err, self._cci)
			logger.debug("Command I=%f", i_new)
			psu.current_setpoint_set(i_new)
