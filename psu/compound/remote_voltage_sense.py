#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# PSU wrapper to provide remote voltage sensed power supply
# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ@zougloub.eu> & contributors
# SPDX-License-Identifier: MIT

import logging
import threading
import time
import contextlib


logger = logging.getLogger(__name__)


__all__ = (
 "psu_in_remote_voltage_sense_with",
)


voltage_setpoints = dict()

voltage_setpoint_gets = dict()

voltage_setpoint_sets = dict()


def _voltage_setpoint_get(self):
	return voltage_setpoints.get(self)

def _voltage_setpoint_set(self, value):
	voltage_setpoints[self] = value


def _run(self, running):
	"""
	"""
	while running:
		try:
			_step(self)
			time.sleep(self._dt)
		except KeyboardInterrupt:
			logger.info("Keyboard interrupt")
			break
		except Exception as e:
			logger.exception("Unexpected error: %s", e)
			break


def _step(self):
	"""
	"""

	v_ref = self.voltage_setpoint_get()
	v_set = voltage_setpoint_gets[self]()
	v_mes = self.sense_voltage()

	logger.info("V got=%f want=%f had_set=%f", v_mes, v_ref if v_ref is not None else -1, v_set)

	if v_ref is None:
		return

	v_err = v_ref - v_mes
	logger.info("Error: %f", v_err)

	if abs(v_err) < self.tol:
		return

	if 0 and psu.in_cc():
		"""
		TODO if the PSU is in CC, then it can never reach the set voltage
		if it is higher than the actual voltage, and the error term would
		raise indefinitely.
		"""

	v_new = v_set + v_err
	if v_new != v_set:
		logger.info("Compensate loss by setting V=%f", v_new)
		voltage_setpoint_sets[self](v_new)


@contextlib.contextmanager
def psu_in_remote_voltage_sense_with(self, sense_voltage_func, dt=1.0, tol=0.001):
	"""
	Context manager that will make a PSU provide a voltage that is remotely sensed,
	when the PSU doesn't support it natively.

	When applied, a background thread is started, which will do remote voltage
	sensing and voltage setpoint adjustment.

	"""

	old_sense_voltage = self.sense_voltage
	self.sense_voltage = sense_voltage_func

	voltage_setpoint_sets[self] = self.voltage_setpoint_set
	self.voltage_setpoint_set = lambda x: _voltage_setpoint_set(self, x)

	voltage_getpoint_gets[self] = self.voltage_getpoint_get
	self.voltage_getpoint_get = lambda: _voltage_getpoint_get(self)

	running = [True]
	step(self)
	self.t = threading.Thread(target=lambda: run(self, running))
	self.t.start()

	yield self

	running[:] = []

	self.t.join()

	self.sense_voltage = old_sense_voltage
	self.voltage_setpoint_get = voltage_setpoint_gets.pop(self)
	self.voltage_setpoint_set = voltage_setpoint_sets.pop(self)

	try:
		del voltage_setpoints[self]
	except KeyError:
		pass

