#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# PSU for a parent PSU single channel

import logging


logger = logging.getLogger(__name__)


class PSU:
	def __init__(self, parent, channel):
		self.parent = parent
		self.channel = channel

	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		pass

	def voltage_setpoint_get(self):
		return self.parent.voltage_setpoint_get(output=self.channel)

	def voltage_setpoint_set(self, value):
		return self.parent.voltage_setpoint_set(value, output=self.channel)

	def current_setpoint_get(self):
		return self.parent.current_setpoint_get(output=self.channel)

	def current_setpoint_set(self, value):
		return self.parent.current_setpoint_set(value, output=self.channel)

	def sense_voltage(self):
		return self.parent.sense_voltage(output=self.channel)

	def sense_current(self):
		return self.parent.sense_current(output=self.channel)
