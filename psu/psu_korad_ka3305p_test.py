import logging
import os
import time
import signal

from ..scpi.scpi_command import SCPI
from .psu_korad_ka3305p import PSU


logger = logging.getLogger(__name__)


cmd = os.environ.get("KA3305P_STDIO_COMMAND",
 "ssh pi4xm socat stdio open:/dev/serial/by-id/usb-USB_Vir_USB_Virtual_COM_NT2009101400-if00,b9600,rawer",
)

def test_enter():
	with SCPI(cmd, eol_rx=b"\x00", eol_tx=b"") as scpi:
		with PSU(scpi) as psu:
			pass

def test_enable():
	with SCPI(cmd, eol_rx=b"\x00", eol_tx=b"") as scpi:
		with PSU(scpi) as psu:
			for i in range(5):
				psu.enable(True)
				psu.enable(False)

def test_setpoint_v():
	with SCPI(cmd, eol_rx=b"\x00", eol_tx=b"") as scpi:
		with PSU(scpi) as psu:
			for sp in range(1, 3000):
				v_ref = sp * 1e-2
				psu.voltage_setpoint_set(v_ref, output=0)
				v_mes = psu.voltage_setpoint_get(output=0)
				logger.info("%f %f", v_ref, v_mes)
				assert v_ref == v_mes

def test_setpoint_i():
	with SCPI(cmd, eol_rx=b"\x00", eol_tx=b"") as scpi:
		with PSU(scpi) as psu:
			for sp in range(1, 5000):
				i_ref = round(sp * 1e-3, 3)
				psu.current_setpoint_set(i_ref, output=0)
				i_mes = psu.current_setpoint_get(output=0)
				logger.info("%f %f", i_ref, i_mes)
				assert i_ref == i_mes

def test_in_enabled():
	for i in range(10):
		try:
			with SCPI(cmd, eol_rx=b"\x00", eol_tx=b"") as scpi:
				with PSU(scpi) as psu:
					psu.voltage_setpoint_set(7.3, output=0)
					psu.current_setpoint_set(0.001, output=0)
					with psu.in_enabled():
						time.sleep(1)
						os.kill(os.getpid(), signal.SIGINT)
						#1/0
		except ZeroDivisionError:
			pass
		except KeyboardInterrupt:
			pass
