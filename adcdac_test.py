#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# ADC/DAC test code
# SPDX-FileCopyrightText: 2025 Jérôme Carretero <cJ-tesmes@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging

import matplotlib.figure

from .adcdac import *

def test():
	vmin = 0
	vmax = 3.3
	nob = 4

	adc = ADC(nob=nob, vmax=vmax, vmin=vmin)
	dac = DAC(nob=nob, vmax=vmax, vmin=vmin)

	logging.getLogger("matplotlib.font_manager").setLevel(logging.INFO)

	v_v = np.linspace(vmin, vmax, (2**nob) * 32)

	fig = matplotlib.figure.Figure(figsize=(10, 6))
	ax = fig.add_subplot(1, 1, 1)

	ax.set_xlabel("Voltage (V)")
	ax.set_ylabel("ADC value (LSB)")

	v_adc = adc(v_v)
	ax.plot(v_v, v_adc,
	 label="ADC",
	)

	v_dac = dac(v_adc)
	ax.plot(v_dac, v_adc,
	 ".",
	 label="DAC",
	)

	ax.set_xlim((vmin, vmax))
	ax.legend()
	ax.grid(True)
	fig.suptitle("ADC/DAC")
	fig.tight_layout()
	fig.savefig("adc.png")
