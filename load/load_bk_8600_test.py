import logging
import contextlib
import os
import time

from ..scpi.scpi_command import SCPI
from .load_bk_8600 import Load
logger = logging.getLogger(__name__)


def test_comm():
	cmd = os.environ.get("BK8600_STDIO_COMMAND")
	with contextlib.ExitStack() as stack:
		scpi = SCPI(cmd, eol=b"\n")
		stack.enter_context(scpi)
		load = Load(scpi)
		stack.enter_context(load)
		logger.info("pouet")

		with load.in_remote():
			with load.in_cc():
				cc = load.setpoint_get()
				logger.info("cc: %s", cc)
				time.sleep(1)
