#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# PSU implementation for B&K Precision 8600-series Programmable Electronic load
# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging
import io
import os
import contextlib
import decimal

with io.open(__file__.replace(".py", ".rst"), "r") as fi:
	__doc__ = fi.read()


logger = logging.getLogger(__name__)


class Load:
	def __init__(self, scpi):
		self.scpi = scpi

	def __enter__(self):
		idn = self.scpi.ask("*IDN?")
		logger.info("IDN: %s", idn)
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		pass

	def setpoint_set(self, value):
		"""
		Configure setpoint (depending on present mode)
		"""
		self.scpi.write(f":{self._func} {value:.3f}{self._unit}")

	def setpoint_get(self):
		"""
		Obtain setpoint (depending on present mode)
		"""
		return float(self.scpi.ask(f":{self._func}?")[:-len(self._unit)])

	def create_measure(kw, unit):
		def get(self):
			f"""
			Measure {unit}
			"""
			return float(self.scpi.ask(f":MEAS:{kw}?"))
		return get

	measure_i = create_measure("CURR", "A")
	measure_u = create_measure("VOLT", "V")
	measure_p = create_measure("POW", "W")

	def _set_mode(self, func):
		self.scpi.write(f":FUNC {func}")
		func_rb = self.scpi.ask(":FUNC?")
		if func_rb != func:
			raise RuntimeError()

	@contextlib.contextmanager
	def backup_and_restore_func(self):
		func_old = self.scpi.ask(":FUNC?")
		yield self
		if func_old == "CONTINUOUS CV":
			self.scpi.ask(":DYN?")
		else:
			self._set_mode(func_old)

	def create_mode(mode, kw, unit):
		@contextlib.contextmanager
		def in_mode(self):
			with self.backup_and_restore_func():
				self._set_mode(mode)
				self._func = kw
				self._unit = unit
				yield self
		return in_mode

	in_cc = create_mode("CURRENT", "CURR", "A")
	in_cv = create_mode("VOLTAGE", "VOLT", "V")
	in_cr = create_mode("RESISTANCE", "RES", "OHM")
	in_cw = create_mode("POWER", "POW", "W")

	def save(self, index: int):
		self.scpi.write(f"*SAV {index}")

	def recall(self, index: int):
		self.scpi.write(f"*RCL {index}")

	def activate(self, doit):
		self.scpi.write(f":INP {1 if doit else 0}")

	def activated(self):
		v = self.scpi.ask(":INP?")
		return int(v)

	def trigger(self):
		self.scpi.write("*TRG")


	@contextlib.contextmanager
	def in_remote(self):
		self.init_remote()
		yield
		self.init_local()

	@contextlib.contextmanager
	def in_local(self):
		self.init_local()
		yield
		# Can't go back
		self.init_local()

	def init_remote(self):
		logger.info("Enter remote control")
		a = self.scpi.write("SYST:REM")

	def init_local(self):
		logger.info("Enter local control")
		a = self.scpi.write("SYST:LOC")

	@contextlib.contextmanager
	def in_activated(self):
		self.activate(True)
		yield
		self.activate(False)

