import logging
import os

import numpy as np

from .rtd import *


logger = logging.getLogger(__name__)


def test():
	for t_ref in np.arange(-200, 661):
		r = t2r(t_ref)
		t_mes = r2t(r)
		logger.info("%s -> %s -> %s", t_ref, r, t_mes)
		assert abs(t_ref - t_mes) < 0.001


def test_cvd():
	preset = PRESETS["PT-385"]
	for t_ref in np.arange(-200, 661):
		r = temperature_to_r_cvd(t_ref, **preset)
		t_mes = r_to_temperature_cvd(r, **preset)
		logger.info("%s -> %s -> %s", t_ref, r, t_mes)
		assert abs(t_ref - t_mes) < 0.001


def test_pt100_tab():
	"""
	Validate computation against tabular data
	"""
	rs = np.loadtxt(os.path.join(os.path.dirname(__file__), "pt100.tab"))
	ts = np.linspace(-200, 850, len(rs))

	drs = rs[1:] - rs[:-1]

	dravg = np.mean(drs)
	drmed = np.median(drs)
	logger.info("1K step resistance variation median=%f mean=%f [ohm/K]", drmed, dravg)

	for t, r, dr in zip(ts[:-1], rs[:-1], drs):
		assert drmed * 3/4 <  dr < drmed * 4/3

	for t, r in zip(ts, rs):
		r_ = t2r(t)
		t_ = r2t(r)
		t_m = r2t(r - 0.005)
		t_p = r2t(r + 0.005)
		assert t_p - t_m < 0.5
		logger.debug("%.3f %.3f(%.3f-%.3f)\t\t%.3f %.3f", t, t_, t_m, t_p, r, r_)
		assert abs(r-r_) < 0.005
		assert abs(t-t_) < max(t_p-t_, t-t_m)

def test_class():
	rtd = RTD()
	for t_ref in np.arange(-200, 661):
		r = rtd.forward(t_ref)
		t_mes = rtd(r)
		logger.info("%s -> %s -> %s", t_ref, r, t_mes)
		assert abs(t_ref - t_mes) < 0.001

