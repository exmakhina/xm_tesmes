#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Thermistor temperature-resistance relations
# SPDX-FileCopyrightText: 2022,2025 Jérôme Carretero <cJ-tesmes@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging

import numpy as np

from .base import ReversibleMapping


logger = logging.getLogger(__name__)

zero_degC = 273.15


def temperature_to_r_sh(t: float,
 a=None, b=None, c=None,
 R0=None, T0=None, B=None,
 log=None, sqrt=None, exp=None,
) -> float:
	"""
	Find resistance from temperature using Steinhart–Hart equation

	:param t: resistance in °C
	:return: resistance
	"""
	if log is None:
		log = np.log
	if sqrt is None:
		sqrt = np.sqrt
	if exp is None:
		exp = np.exp

	if a is None:
		a = 1/T0 - (1/B) * log(R0)
	if b is None:
		b = 1/B
	if c is None:
		c = 0

	T = t + zero_degC

	if c == 0:
		return exp((1-a*T)/(b*T))

	x = 1/c * (a - 1/T)
	y = sqrt((b/(3*c))**3 + (x**2)/4)
	return exp((y - x/2)**(1/3) - (y + x/2)**(1/3))


def r_to_temperature_sh(R: float,
 a=None, b=None, c=None,
 R0=None, T0=None, B=None,
 log=None,
) -> float:
	"""
	Find temperature from resistance using analytic solution to Steinhart–Hart equation

	:param R: measured resistance
	:return: temperature in °C
	"""
	if log is None:
		log = np.log

	if a is None:
		a = 1/T0 - (1/B) * log(R0)
	if b is None:
		b = 1/B
	if c is None:
		c = 0

	T = 1 / (a + b * log(R) + c * log(R)**3)

	return T - zero_degC


r2t = r_to_temperature_sh
t2r = temperature_to_r_sh


class Thermistor(ReversibleMapping):
	"""
	Class interface

	Use __call__ to do forward and .reverse to do reverse
	"""
	def __init__(self, forward_reverse=None,
	  a=None, b=None, c=None,
	  R0=None, T0=None, B=None,
	  log=None, sqrt=None, exp=None,
	 ):
		if log is None:
			log = np.log

		if a is None:
			a = 1/T0 - (1/B) * log(R0)
		if b is None:
			b = 1/B
		if c is None:
			c = 0

		if forward_reverse is None:
			forward = lambda t: temperature_to_r_sh(t,
			 a=a,b=b,c=c,
			 log=log, sqrt=sqrt, exp=exp,
			)
			reverse = lambda r: r_to_temperature_sh(r,
			 a=a,b=b,c=c,
			 log=log,
			)
		else:
			forward, reverse = forward_reverse
		self.forward = forward
		self.reverse = reverse
