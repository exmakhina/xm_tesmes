#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-tesmes@zougloub.eu>
# SPDX-License-Identifier: MIT
# Transducer base stuff


class ReversibleMapping:
	"""
	Transducers map one physical quantity to another.
	Mathematically, these mappings are smooth bijections between subsets of R,
	mostly diffeomorphisms.
	The physical transduction processes may not be reversible.
	"""

	def forward(self, value):
		"""
		Simulate transducer action
		"""
		raise NotImplementedError()

	def reverse(self, value):
		"""
		Reverse transducer action
		"""
		raise NotImplementedError()

	def __call__(self, value):
		"""
		When you use a transducer, usually you want is reverse
		"""
		return self.reverse(value)


class Gain(ReversibleMapping):
	def __init__(self, k):
		"""

		:param k: *forward* gain

		Example: for a shunt giving 10 mV per A, use k=10e-3;
		use __call__ to find A given a voltage measurement in V.

		"""
		self._k = k
		self._1_k = 1/k

	def forward(self, value):
		return value * self._k

	def reverse(self, value):
		return value * self._1_k

