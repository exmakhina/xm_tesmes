#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022,2024 Jérôme Carretero <cJ-tesmes@zougloub.eu>
# SPDX-License-Identifier: MIT
# Thermocouple temperature-emf relations

import sys, io, os
import logging

from .tabular import (
 bisect,
 forward,
 reverse,
)

from .base import ReversibleMapping

logger = logging.getLogger(__name__)


TABLES = {}


def get_table(type):
	if (table := TABLES.get(type)) is not None:
		return table

	tab = os.path.join(os.path.dirname(__file__), f"type_{type}.tab")

	table = {}
	with io.open(tab, "r", encoding="iso8859-15") as fi:
		for line in fi:
			line = line.strip()

			if set(line) == set("*"):
				break

			try:
				a, *b = line.split()
			except ValueError:
				continue

			logger.debug("Line: %s %s", a, b)

			if a == "°C":
				offsets = [ float(x) for x in  b ]
				continue

			try:
				int(a)
			except ValueError:
				continue

			if len(b) > 1:
				t = float(a)
				vals = [ float(x)*1e-3 for x in b ]
				logger.debug("Vals: %s", vals)
				for idx_val, val in enumerate(vals):
					table[t+offsets[idx_val]] = val

	TABLES[type] = [ (k,v) for (k,v) in sorted(table.items()) ]
	return TABLES[type]


def temperature_to_emf_type_k(t_degC: float) -> float:
	"""
	:param t_degC: temperature in °C
	:return: EMF in V
	"""
	table = get_table("k")
	return forward(table, t_degC)


def emf_to_temperature_type_k(v: float) -> float:
	"""
	:param v: EMF in V
	:return: temperature in °C
	"""
	table = get_table("k")
	return reverse(table, v)


class Thermocouple(ReversibleMapping):
	def __init__(self, kind="k", table=None):
		if table is not None:
			self.table = table
			self.kind = kind
		else:
			self.kind = kind
			self.table = get_table(kind)

	def forward(self, t_degC):
		return forward(self.table, t_degC)

	def reverse(self, v):
		return reverse(self.table, v)

