#!/usr/bin/env python
# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-tesmes@zougloub.eu>

import typing as t
import os
import sys
import code
import signal
import logging
import argparse


logger = logging.getLogger(__name__)




def main(argv=None):
	parser = argparse.ArgumentParser(
	 description="Test",
	)

	log_level = os.environ.get("TESMES_XDUCER_LOG_LEVEL", "WARNING")

	parser.add_argument("--log-level",
	 default=log_level,
	 help="Logging level (see Python logging docs); default {}".format(log_level),
	)

	parser.add_argument("--color",
	 type=int,
	 choices=[0,1],
	)

	subparsers = parser.add_subparsers(
	 help=f'the command; type "transducers -h" for command-specific help',
	 dest="command",
	)


	subp = subparsers.add_parser(
	 "thermocouple",
	 help="",
	)

	subp.add_argument("--type",
	 help="Thermocouple type",
	 choices=("k", "s"),
	 default="k",
	)

	subparsers_tc = subp.add_subparsers(
	 help=f'TC command; type "transducers thermocouple -h" for command-specific help',
	 dest="tc_command",
	)

	subp = subparsers_tc.add_parser(
	 "v2t",
	 help="",
	)

	subp.add_argument("v",
	 help="",
	 type=float,
	)

	def doit(args):
		from .thermocouple import (get_table, reverse)
		table = get_table(args.type)
		res = reverse(table, args.v)
		print(res)

	subp.set_defaults(func=doit)


	subp = subparsers.add_parser(
	 "rtd",
	 help="",
	)

	subp.add_argument("--type",
	 help="RTD type",
	 choices=("PT100", "PT200", "PT1000"),
	 default="PT100",
	)

	subparsers_tc = subp.add_subparsers(
	 help=f'RTD command; type "transducers rtd -h" for command-specific help',
	 dest="rtd_command",
	)

	subp = subparsers_tc.add_parser(
	 "r2t",
	 help="",
	)

	subp.add_argument("r",
	 help="",
	 type=float,
	)

	def doit(args):
		from .rtd import RTD
		rtd = RTD()
		res = rtd(args.r)
		print(res)

	subp.set_defaults(func=doit)


	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except ImportError:
		pass

	args = parser.parse_args(argv)

	try:
		import coloredlogs
		coloredlogs.install(
		 level=getattr(logging, args.log_level),
		 logger=logging.root,
		 isatty=args.color,
		)
	except ImportError:
		logging.basicConfig(
		 datefmt="%Y%m%dT%H%M%S",
		 level=getattr(logging, args.log_level),
		 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
		)

	annoying_loggers = []
	for name in annoying_loggers:
		logging.getLogger(name).setLevel(logging.WARNING)

	if getattr(args, "func", None) is None:
		parser.print_help()
		return 1

	return args.func(args)


if __name__ == "__main__":
	res = main()
	raise SystemExit(res)
