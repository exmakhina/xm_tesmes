import logging
import os

import numpy as np

from .thermistor import *


logger = logging.getLogger(__name__)


def test():
	a = 1.40e-3
	b = 2.37e-4
	c = 9.90e-8

	for t_ref in np.arange(-90, 130):
		r = t2r(t_ref, a=a, b=b, c=c)
		t_mes = r2t(r, a=a, b=b, c=c)
		logger.info("%s -> %s -> %s", t_ref, r, t_mes)
		assert abs(t_ref - t_mes) < 0.001


def test_class():
	a = 1.40e-3
	b = 2.37e-4
	c = 9.90e-8
	thermistor = Thermistor(a=a, b=b, c=c)

	for t_ref in np.arange(-90, 130):
		r = thermistor.forward(t_ref)
		t_mes = thermistor(r)
		logger.info("%s -> %s -> %s", t_ref, r, t_mes)
		assert abs(t_ref - t_mes) < 0.001

