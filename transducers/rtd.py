#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# RTD (eg. PT100) temperature-resistance relations
# SPDX-FileCopyrightText: 2022,2024 Jérôme Carretero <cJ-tesmes@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging

from .base import ReversibleMapping


logger = logging.getLogger(__name__)


PRESETS = {
 "PT-385": {
  "R0": 100.0000,
  "A": 3.9083E-3,
  "B": -5.775E-7,
  "C": -4.183E-12,
 },
}

def temperature_to_r_cvd(t: float, R0=100, A=3.9083E-3, B=-5.775E-7, C=-4.183E-12) -> float:
	"""
	Find resistance from temperature using Callendar–Van Dusen equation
	"""
	if t < 0:
		return R0 * (1 + A * t + B * t**2 + C * (t-100) * (t**3))
	return R0 * (1 + A * t + B * t**2)


def r_to_temperature_cvd(Rt: float, R0=100, A=3.9083E-3, B=-5.775E-7, C=-4.183E-12) -> float:
	"""
	Find temperature from resistance using analytic solution to Callendar–Van Dusen equation

	:param Rt: measured resistance
	:return: temperature in °C

	Note that the resistance value must be in the valid input domain...
	"""

	sqrt = lambda x: x ** 0.5
	if Rt >= R0:
		return -A/(2*B) + sqrt(A**2*R0 - 4*B*R0 + 4*B*Rt)/(2*B*sqrt(R0))
	x0 = 1/C
	x1 = B*x0
	x2 = x1 - 3750
	x3 = x2**3
	x4 = A*x0
	x5 = (50*x1 + x4 - 125000)**2
	x6 = x0 + 25*x4 - Rt*x0/R0
	x7 = 625*x1 + x6 - 1171875
	x8 = -B**2/(12*C**2) - x6
	x9 = (-x2*x7/6 + x3/216 + x5/16 + sqrt(x8**3/27 + (x2*x7/3 - x3/108 - x5/8)**2/4))**(1/3)
	x10 = 2*x9
	x11 = 2*x8/(3*x9)
	x12 = sqrt(-2*x1/3 + x10 - x11 + 2500)
	return x12/2 - sqrt(-4*x1/3 - x10 + x11 + 5000 - (100*x1 + 2*x4 - 250000)/x12)/2 + 25

r2t = r_to_temperature_cvd
t2r = temperature_to_r_cvd


class RTD(ReversibleMapping):
	"""
	Class interface

	Use __call__ to do forward and .reverse to do reverse
	"""
	def __init__(self, forward_reverse=None):
		if forward_reverse is None:
			forward = temperature_to_r_cvd
			reverse = r_to_temperature_cvd
		else:
			forward, reverse = forward_reverse
		self.forward = forward
		self.reverse = reverse
