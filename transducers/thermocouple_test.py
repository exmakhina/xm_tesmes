import logging

import numpy as np

from .thermocouple import *


logger = logging.getLogger(__name__)


def test_type_k():
	for t_ref in np.arange(-250, 500, 0.1):
		v = temperature_to_emf_type_k(t_ref)
		logger.debug("v=%f", v)
		t_mes = emf_to_temperature_type_k(v)
		logger.debug("t=%f wanted %f", t_mes, t_ref)
		assert abs(t_mes-t_ref) < 0.05


def test_type_s():
	table = get_table("s")
	for t_ref in np.arange(-50, 1768.1, 1):
		v = forward(table, t_ref)
		logger.info("v=%f", v)
		t_mes = reverse(table, v)
		logger.info("t=%f wanted %f", t_mes, t_ref)
		assert abs(t_mes-t_ref) < 0.05

def test_class():
	tc = Thermocouple()
	for t_ref in np.arange(-50, 1372, 1):
		v = tc.forward(t_ref)
		#logger.info("v=%f", v)
		t_mes = tc(v)
		#logger.info("t=%f wanted %f", t_mes, t_ref)
		assert abs(t_mes-t_ref) < 0.05
