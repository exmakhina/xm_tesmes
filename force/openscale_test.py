#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# OpenScale scale driver test

import logging
import contextlib
import os
import time

import numpy as np

from ..scpi.scpi_command import SCPI

from .openscale import Scale


logger = logging.getLogger(__name__)


cmd = os.environ.get("OPENSCALE_STDIO_COMMAND",
 "socat open:/dev/serial/by-id/usb-FTDI_FT231X_USB_UART_D309SW44-if00-port0,rawer,b115200 stdio"
)

def test_basic():
	with contextlib.ExitStack() as stack:
		scpi = SCPI(cmd, eol=b"\n")
		stack.enter_context(scpi)
		logger.info("pouet")
		ser = scpi.ser

		meter = Scale(ser)
		stack.enter_context(meter)

		ts = []
		vs = []
		while True:
			try:
				v, ext = meter.measure_immediate_ext()
				t = ext["t"]
				logger.info("got @%s: %s", t, v)
				ts.append(t)
				vs.append(v)
			except KeyboardInterrupt:
				break

		logger.info("done")

	ts = np.array(ts)
	vs = np.array(vs)

	ts -= ts[0]

	import matplotlib
	import matplotlib.figure
	import matplotlib.patches
	import matplotlib.mlab as mlab
	import matplotlib.backend_bases

	subpcfg = matplotlib.figure.SubplotParams(
	 left  =0.10,
	 bottom=0.10,
	 right =0.90,
	 top   =0.90,
	 wspace=0.00,
	 hspace=0.00,
	)
	figure = matplotlib.figure.Figure(
	 facecolor='white',
	 edgecolor='white',
	 subplotpars=subpcfg,
	)

	figure.set_figheight(8)
	figure.set_figwidth(12)
	figure.set_dpi(72)

	axes = figure.add_subplot(1, 1, 1)

	axes.xaxis.grid(True)
	axes.yaxis.grid(True)
	axes.set_xlabel("Time (s)")
	axes.set_ylabel("Weight (kg)")

	axes.plot(ts, vs)

	#axes.legend()

	title = "Scale drift"

	figure.suptitle(title)

	out_dir = "."
	base = f"test"
	for ext in ("svg", "png", "pdf"):
		canvas_class = matplotlib.backend_bases.get_registered_canvas_class(ext)
		figure_canvas = canvas_class(figure)
		canvas_print = getattr(figure_canvas, 'print_%s' % ext)
		canvas_print("%s.%s" % (base, ext))

