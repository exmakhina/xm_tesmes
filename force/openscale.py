#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# OpenScale scale driver

import io
import re
import time
import sys
import logging
import queue
import threading


with io.open(__file__.replace(".py", ".rst"), "r") as fi:
	__doc__ = fi.read()


logger = logging.getLogger(__name__)


class Scale():
	"""
	"""
	def __init__(self, ser: "SerialPipe"):
		self.queue = queue.Queue()
		self.ser = ser

	def __enter__(self):
		self.running = True
		self._last = b""
		self.t = threading.Thread(target=self.run)
		self.t.start()
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		self.running = False
		self.t.join()

	def measure_immediate_ext(self):
		now = time.monotonic()

		while True:
			if not self.running:
				raise RuntimeError("stopped")

			x = self.queue.get()
			if x is None:
				raise RuntimeError("stopped")

			t, v = x

			if t < now:
				continue

			return v["w"], v


	def measure_immediate(self):
		v, extra = self.measure_immediate_ext()
		return v

	def step(self):
		line = self.ser.read_until(b"\r\n")
		now = time.monotonic()
		logger.debug("Line: %s", line)

		try:
			#ts,
			w, u, temp, e = line.decode().split(",")
		except:
			return

		w = float(w)

		res = dict(
		 t=now,
		 w=w,
		)

		#logger.info("Res: %s", res)

		self.queue.put((now, res))

	def run(self):
		while self.running:
			try:
				self.step()
			except Exception as e:
				logger.exception("Error while stepping: %s", e)
