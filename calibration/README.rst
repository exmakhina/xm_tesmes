.. SPDX-FileCopyrightText: 2017,2024 Jérôme Carretero <cJ-tesmes@zougloub.eu> & contributors
.. SPDX-License-Identifier: CC-BY-SA-4.0


###########
Calibration
###########



Instrument Calibration Metadata
###############################


Design
******

We'd want calibration metadata to contain:

- if relevant, reference to tools that were used

  - if relevant, their calibration status

- rather than storing specific compensation factors,
  use measurements and their uncertainties, that were used to find
  them, and eventually different compensation methods.




Implementation
**************

Human-editable.

.. code:: yaml

  calibration:
    dut: MCC ...
    time: 2024-08-14 23:12:15
    psu: KORAD KA3305P N/A
    voltmeter: FLUKE 8845A 2811010

    channel 0:

    - series:

        ref: []
        mes: []
        std: []


Calibration Procedures
######################


DAQ Clock Calibrations
**********************

TL;DR:

- Keep in mind that sampling rates are set using clock scaling,
  and in this case they can't be arbitrary.

  Make sure to know the actual sampling rate resulting from
  clock scaling, which might not be the command; ideally only use
  scales corresponding to round prescaler/counter values.

- Using a square signal with long and stable period, such as a GPS PPS pulse,
  is nice.

- Measure (repeatedly) period between N edges, expressed in samples,
  allowing, knowing the pulse frequency, to obtain mean and std.


ADC Calibrations
****************

TL;DR:

- Validate eventual hysteresis

- Static:

  - Set input value, measure using more precise meter,
    obtaining reference mean+std of signal,
    and ideally ensuring it's much lower than 1 LSB.

  - Obtain ADC counts, compute mean+std.


Transducer Calibrations
***********************

TL;DR:

- Validate eventual hysteresis

- Static:

  - Set input value, measure, obtaining/estimating mean+std of signal

  - Measure output value, obtaining mean+std of signal

