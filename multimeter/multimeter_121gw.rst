########################
EEVBlog 121GW multimeter
########################


:Homepage: https://www.eevblog.com/product/121gw/


Communication
#############


Enable/disable Bluetooth by holding down "1ms PEAK" button.


Issues
######

As of firmware v2.05, there is an issue when the multimeter
is used in automatic range mode and the range changes.
While the screen is dashed or blanked, to hide invalid measurements,
they are still sent over bluetooth, with no flag indicating that
they shouldn't be used.


Implementation
##############

TODO complete implementation - not all modes are supported

