############################
Fluke 8845A/8846A Multimeter
############################


Notes:

- It only takes a network IP at boot
- Fetch2 takes a single measurement

References:

- https://us.flukecal.com/category/literature-type/product-manuals
- https://us.flukecal.com/literature/product-manuals/8845a8846a-users-manual
- https://us.flukecal.com/literature/product-manuals/8845a8846a-programmers-manual
