#!/usr/bin/env python
# -*- vi: noet
# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-tesmes@zougloub.eu>

import logging
import time
import os

import numpy as np
import serial

from .multimeter_chargerlab_powerz import KM002CDevice


logger = logging.getLogger(__name__)


default_port = "/dev/serial/by-id/usb-ChargerLab_POWER-Z_KM003C_037376-if01"


def test_interface():
	port = os.environ.get("POWERZ_PORT", default_port)
	ser = serial.Serial(
	 port=port,
	)

	v_t = []
	v_v = []
	v_i = []
	v_p = []
	with ser:
		with KM002CDevice(ser) as meter:
			for i in range(30):
				adc_data = meter.get_adc_data()
				logger.info("n: %s", adc_data.n)
				logger.info("Rate: %s", adc_data.Rate)

def test_power():
	port = os.environ.get("POWERZ_PORT", default_port)
	ser = serial.Serial(
	 port=port,
	)

	v_t = []
	v_p = []
	with ser:
		with KM002CDevice(ser) as meter:
			for i in range(1000):
				p = meter.measure_immediate()
				v_t.append(time.time())
				v_p.append(p)

	v_t = np.array(v_t)
	v_t -= v_t[0]

	import matplotlib.figure

	logging.getLogger("matplotlib.font_manager").setLevel(logging.WARNING)

	fig = matplotlib.figure.Figure(figsize=(12,9))
	ax = fig.add_subplot(1, 1, 1)

	ax.plot(v_t, v_p,
	 ".",
	)

	title = f"Test"

	ax.grid(True)
	ax.legend()
	ax.set_xlabel("Time (s)")
	#ax.set_ylim((4, 6))
	ax.set_ylabel("Power (W)")
	fig.suptitle(title)

	basename = __name__.split(".")[-1]
	fig.savefig(f"{basename}-power.png")



def test_adv():
	port = os.environ.get("POWERZ_PORT", default_port)
	ser = serial.Serial(
	 port=port,
	)

	v_t = []
	v_v = []
	v_i = []
	v_p = []
	with ser:
		with KM002CDevice(ser) as meter:
			for i in range(1000):
				adc_data = meter.get_adc_data()
				#logger.info("Data: %s", adc_data)
				#adc_data.display()
				v_t.append(time.time())
				v_v.append(adc_data.Vbus)
				v_i.append(adc_data.Ibus)
				v_p.append(adc_data.Vbus*adc_data.Ibus)

	v_t = np.array(v_t)
	v_t -= v_t[0]

	import matplotlib.figure

	logging.getLogger("matplotlib.font_manager").setLevel(logging.WARNING)

	fig = matplotlib.figure.Figure(figsize=(12,9))
	ax = fig.add_subplot(1, 1, 1)


	color = 'tab:blue'
	ax.set_xlabel("Time (s)")
	ax.set_ylabel("Voltage (V)", color=color)
	ax.tick_params(axis="y", labelcolor=color)
	ax.set_ylim((0, 6))
	ax.plot(v_t, v_v,
	 ".",
	)

	ax2 = ax.twinx()
	color = 'tab:red'
	ax2.set_ylabel("Current (A)", color=color)
	ax2.plot(v_t, v_i,
	 ".",
	 color=color,
	)
	ax2.tick_params(axis="y", labelcolor=color)
	ax2.set_ylim((0, 2))

	ax.grid(True)
	#ax.legend()

	title = f"Test"
	fig.suptitle(title)

	basename = __name__.split(".")[-1]
	fig.savefig(f"{basename}-adv.png")
