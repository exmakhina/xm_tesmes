import logging
import time
import io

from .multimeter_captty import Multimeter


logger = logging.getLogger(__name__)


def test():
	captty_file = __file__.replace(".py", ".cast")
	with Multimeter(captty_file, key="a") as m:
		m.now = 0
		x = m.measure_immediate()
		logger.info("X: %s", x)
		m.now = m.then + 0.1
		x = m.measure_immediate()
		logger.info("X: %s", x)
