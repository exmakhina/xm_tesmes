#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Multimeter implementation for sigrok multimeters

"""

References:

- https://github.com/karlp/sigrok-python-example/blob/master/sr-read-methods.py

"""

import sys, io, os, subprocess
import re
import logging
import time, datetime
import threading
import queue
import collections

import sigrok.core as sr


logger = logging.getLogger(__name__)


class Multimeter:
	"""
	"""
	def __init__(self, option_str="eevblog-121gw:conn=bt/ble122/88-6B-0F-81-AA-48"):
		self.queues = collections.defaultdict(queue.Queue)
		self.option_str = option_str

	def __enter__(self):
		self.running = True
		self.context = sr.Context.create()
		logger.debug("Running with libsigrok %s/%s",
		 self.context.package_version,
		 self.context.lib_version,
		)

		#self.context.log_level = sr.LogLevel.INFO

		driver, options = self.option_str.split(":", 1)
		d = self.context.drivers[driver] # TODO: check it found one
		d_opts = dict()
		for kv in options.split(":"):
			logger.info("Option %s", kv)
			x = kv.split("=", 1)
			k, v = kv.split("=", 1)
			d_opts[k] = v
		logger.info("Options: %s", d_opts)

		while True:
			devices = d.scan(**d_opts)
			if not devices:
				logger.info("Waiting for devices...")
				time.sleep(1)
				continue
			break
		self._firstchannel = None
		self.device = devices[0]
		self.device.open()
		self.session = self.context.create_session()
		self.session.add_device(self.device)
		self.t = threading.Thread(target=self.run)
		self.t.start()
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		self.running = False
		if self.session.is_running():
			self.session.stop()
		self.t.join()
		self.device.close()

	def _callback(self, dev, p):
		logger.debug("Callback %s %s", dev, p)
		now = time.monotonic()
		if p.type != sr.PacketType.ANALOG:
			return
		logger.debug("channels: %d", len(p.payload.channels))
		payload = p.payload
		logger.debug("measured quantity: %s", payload.mq)
		logger.debug("payload: %s %s", p.payload.data, p.payload.unit)
		for chan, value in zip(payload.channels, payload.data):
			logger.debug("channel: %s %s %s", chan.type, chan.name, value)
			self.queues[chan.name].put((now, value[0], str(payload.unit)))
			if self._firstchannel is None:
				self._firstchannel = chan.name

	def run(self):
		try:
			self.session.start()
			self.session.add_datafeed_callback(self._callback)
			self.session.run()
		except Exception as e:
			logger.exception("Exception: %s", e)
			self.running = False
			for k, v in self.queues:
				v.put(None)

	def measure_immediate_ext(self):
		now = time.monotonic()
		while True:
			if not self.running:
				raise RuntimeError("stopped")

			if self._firstchannel is None:
				logger.debug("Waiting for channels")
				time.sleep(0.1)
				continue

			v = self.queues[self._firstchannel].get()

			if v is None:
				raise RuntimeError("stopped")

			t, v, u = v

			extra = dict(
			 t=t,
			 unit=u,
			)

			if t > now:
				return v, extra

	def measure_immediate(self):
		v, extra = self.measure_immediate_ext()
		return v
