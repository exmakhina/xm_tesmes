import logging
import contextlib
import os
import time

from ..scpi.scpi_command import SCPI
from .multimeter_keysight_truevolt import Multimeter

logger = logging.getLogger(__name__)


def test_streaming():

	cmd = os.environ.get("MULTIMETER_TRUEVOLT_STDIO_COMMAND",
	)

	with contextlib.ExitStack() as stack:
		scpi = SCPI(cmd, eol=b"\n")
		stack.enter_context(scpi)
		meter = Multimeter(scpi)
		stack.enter_context(meter)
		logger.info("pouet")

		ins = scpi

		ins.write("ABORT")
		ins.write("TRIG:SOUR BUS")
		ins.write("TRIG:COUN 1")
		ins.write("SAMP:COUN 1000000000")

		if 0:
			ins.write("SAMP:SOUR TIM")
			ins.write("SAMP:TIM 0.0001")
		else:
			ins.write("TRIG:DEL MIN")
			ins.write("SAMP:SOUR IMM")

		ins.write("INIT")
		ins.write("*TRG")

		n = 0
		t0 = time.time()
		while n < 200000:
			m = meter.ask_r()
			if 0:
				for e in m:
					logger.info("x: %f", e)
					n += 1
			else:
				logger.info("m: %d", len(m))
				n += len(m)
			#time.sleep(0.01)
		t1 = time.time()
		logger.info("%f", n / (t1 - t0))
