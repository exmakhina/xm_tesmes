#!/usr/bin/env python
# -*- vi: noet
# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-tesmes@zougloub.eu>

import logging
import struct
import time
from enum import IntEnum


logger = logging.getLogger(__name__)

 # Structures and registers

class CmdCtrlMsgType(IntEnum):
	"""
	enum cmd_ctrl_msg_type
	"""
	CMD_SYNC = 1
	CMD_CONNECT = 2
	CMD_DISCONNECT = 3
	CMD_RESET = 4
	CMD_ACCEPT = 5
	CMD_REJECT = 6
	CMD_FINISHED = 7
	CMD_JUMP_APROM = 8
	CMD_JUMP_DFU = 9
	CMD_GET_STATUS = 10
	CMD_ERROR = 11
	CMD_GET_DATA = 12
	CMD_GET_FILE = 13

class CmdDataMsgType(IntEnum):
	"""
	enum cmd_data_msg_type
	"""
	CMD_HEAD = 64
	CMD_PUT_DATA = 65

class AttributeDataType(IntEnum):
	"""
	enum  attribute_data_type
	"""
	ATT_ADC = 0x001
	ATT_ADC_QUEUE = 0x002
	ATT_ADC_QUEUE_10K = 0x004
	ATT_SETTINGS = 0x008
	ATT_PD_PACKET = 0x010
	ATT_PD_STATUS = 0x020
	ATT_QC_PACKET = 0x040


class MsgHeader:
	"""
	Following the design of MsgHeader_TypeDef...
	"""
	def __init__(self, object=0):
		self.object = object
		self.type = 0
		self.extend = 0
		self.id = 0
		self.att = 0
		self.obj = 0
		self.next = 0
		self.chunk = 0
		self.size = 0

	def pack_ctrl(self, type, extend, id, att):
		self.type = type & 0x7F
		self.extend = extend & 0x01
		self.id = id & 0xFF
		self.att = att & 0x7FFF
		self.object = (self.type |
					   (self.extend << 7) |
					   (self.id << 8) |
					   (self.att << 17))

	def unpack_ctrl(self):
		self.type = self.object & 0x7F
		self.extend = (self.object >> 7) & 0x01
		self.id = (self.object >> 8) & 0xFF
		self.att = (self.object >> 17) & 0x7FFF

	def pack_data(self, type, extend, id, obj):
		self.type = type & 0x7F
		self.extend = extend & 0x01
		self.id = id & 0xFF
		self.obj = obj & 0x3FF
		self.object = (self.type |
					   (self.extend << 7) |
					   (self.id << 8) |
					   (self.obj << 16))

	def unpack_data(self):
		self.type = self.object & 0x7F
		self.extend = (self.object >> 7) & 0x01
		self.id = (self.object >> 8) & 0xFF
		self.obj = (self.object >> 16) & 0x3FF

	def pack_header(self, att, next, chunk, size):
		self.att = att & 0x7FFF
		self.next = next & 0x01
		self.chunk = chunk & 0x3F
		self.size = size & 0x3FF
		self.object = ((self.att) |
					   (self.next << 15) |
					   (self.chunk << 16) |
					   (self.size << 22))

	def unpack_header(self):
		self.att = self.object & 0x7FFF
		self.next = (self.object >> 15) & 0x01
		self.chunk = (self.object >> 16) & 0x3F
		self.size = (self.object >> 22) & 0x3FF

	def to_bytes(self):
		return struct.pack('<I', self.object)

	@classmethod
	def from_bytes(cls, data):
		if len(data) < 4:
			raise ValueError("Not enough data to unpack MsgHeader")
		object, = struct.unpack('<I', data[:4])
		return cls(object=object)

class AdcData:
	"""
	AdcData_TypeDef
	"""
	def __init__(self, data_bytes):
		if len(data_bytes) < 40:
			raise ValueError("Not enough data to unpack AdcData")

		unpacked = struct.unpack('<6i6H4B', data_bytes[:40])
		self.Vbus = unpacked[0] * 1e-6
		self.Ibus = unpacked[1] * 1e-6
		self.Vbus_avg = unpacked[2] * 1e-6
		self.Ibus_avg = unpacked[3] * 1e-6
		self.Vbus_ori_avg = unpacked[4] * 1e-6
		self.Ibus_ori_avg = unpacked[5] * 1e-6
		self.Temp = unpacked[6] * 1e-2
		self.Vcc1 = unpacked[7] * 1e-4
		self.Vcc2 = unpacked[8] * 1e-4
		self.Vdp = unpacked[9] * 1e-4
		self.Vdm = unpacked[10] * 1e-4
		self.Vdd = unpacked[11] * 1e-4
		rate_and_n = unpacked[12:]
		self.Rate = rate_and_n[0] & 0x03
		self.n = rate_and_n[1:]

 # Interface

class KM002CDevice:
	"""
	Multimeter interface for KM002C / KM003C USB power meter
	"""

	def __init__(self, ser, timefunc=None):
		"""
		:param ser: something that behaves like a serial port

		Note: for max performance, to reduce latency, use a real serial port.
		"""
		self.ser = ser
		if timefunc is None:
			timefunc = time.monotonic()
		self.timefunc = timefunc

	def __enter__(self):
		self.ser.read(self.ser.inWaiting())
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		pass

	def send_command(self, cmd_type, att, id=0):
		"""
		Create a MsgHeader object and pack it
		"""
		header = MsgHeader()
		header.pack_ctrl(type=cmd_type, extend=0, id=id, att=att)
		cmd_bytes = header.to_bytes()
		logger.debug("> %s", cmd_bytes.hex())
		self.ser.write(cmd_bytes)

	def receive_response(self, expected_length=48):
		"""
		Read data from the device
		"""
		data = self.ser.read(expected_length)
		logger.debug("< %s", data.hex())
		return data

	def parse_message(self, data):
		if len(data) < 4:
			raise ValueError("Not enough data to parse message header")

		header = MsgHeader.from_bytes(data[:4])
		header.unpack_ctrl()
		match header.type:
			case CmdDataMsgType.CMD_PUT_DATA.value:
				header_ext = MsgHeader.from_bytes(data[4:8])
				header_ext.unpack_header()
				if header_ext.att == AttributeDataType.ATT_ADC:
					return AdcData(data[8:])
				else:
					raise NotImplementedError(data)
			case _:
				raise NotImplementedError(data)

	def get_adc_data(self):
		self.send_command(cmd_type=CmdCtrlMsgType.CMD_GET_DATA, att=AttributeDataType.ATT_ADC)
		data = self.receive_response(expected_length=52)
		if len(data) < 52:
			raise ValueError(f"Incomplete data received ({len(data)}")
		return self.parse_message(data)

	def measure_immediate_ext(self):
		t = self.timefunc()
		adc_data = self.get_adc_data()
		power = adc_data.Vbus * adc_data.Ibus
		extra = dict(
		 t=t,
		 adc_data=adc_data,
		)
		return power, extra

	def measure_immediate(self):
		adc_data = self.get_adc_data()
		return adc_data.Vbus * adc_data.Ibus
