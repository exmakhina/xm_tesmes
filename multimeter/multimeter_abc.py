#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Multimeter base class

"""
A basic multimeter at least senses one thing, immediately.

There is no particular notion of time of measurement,
except we know an immediate measurement should return
right after an acquisition was made.

There is no particular notion of sensing integration rate,
or sampling rate, it is implicit.
"""


import time


logger = logging.getLogger(__name__)


class Multimeter():
	"""
	"""
	def __init__(self, unit):
		self._unit = unit

	def __enter__(self):
		return self

	def __exit__(self, ext_type, exc_value, exc_traceback):
		pass

	def measure_immediate_ext(self):
		extra = dict(
		 unit=self._unit,
		 t_beg=time.time(),
		)
		return float("NaN"), extra

	def measure_immediate(self):
		"""
		Immediate measurement returns the read value.
		"""
		return float("NaN")
