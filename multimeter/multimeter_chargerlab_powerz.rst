##################################
CHARGERLAB POWER-Z USB Power Meter
##################################

:Homepage: https://www.chargerlab.com/category/power-z/power-z-km003c-km002c/


A cool and tiny USB power meter, featuring:

- a super-capacitor and an external communication / supply interface;
- a TI INA228 power metering chip;
- 1000 sps readings;
- Software interface using USB CDC & HID with reasonably-well
  documented interface
- USB-C diagnostics
- USB charging detection
