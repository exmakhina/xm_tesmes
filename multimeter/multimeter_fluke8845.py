#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Multimeter implementation for the Fluke network multimeter

import io
import os
import time, socket, re, datetime, sys
import logging
import contextlib

from ..nested_context_mixin import NestedContextMixin


with io.open(__file__.replace(".py", ".rst"), "r") as fi:
	__doc__ = fi.read()


logger = logging.getLogger(__name__)


class Multimeter(NestedContextMixin):
	"""
	"""
	def __init__(self, scpi, timefunc=None):
		NestedContextMixin.__init__(self)
		self.scpi = scpi
		if timefunc is None:
			timefunc = time.monotonic
		self.timefunc = timefunc

	def init(self):
		"""
			Performs initialization of the meter.
		"""
		idn = self.scpi.ask("*IDN?")
		return self

	def exit(self, exc_type, exc_value, exc_tb):
		pass

	@contextlib.contextmanager
	def in_remote(self):
		self.init_remote()
		yield
		self.init_local()

	@contextlib.contextmanager
	def in_display_active(self, value):
		value = int(value)
		old = self.scpi.ask("DISP?")
		self.scpi.write(f"DISP {value}")
		yield
		self.scpi.write(f"DISP {old}")

	@contextlib.contextmanager
	def in_local(self):
		self.init_local()
		yield
		# Can't go back
		self.init_local()

	def init_remote(self):
		logger.info("Enter remote control")
		a = self.scpi.write("SYST:REM")

	def init_local(self):
		logger.info("Enter local control")
		a = self.scpi.write("SYST:LOC")

	def abort(self):
		self.scpi.write("")

	def streaming_read(self):
		"""
		Perform raw stream reading that was initiated using READ?,
		giving max. 50k measurements.
		"""
		self.scpi.write("READ?")
		eof = False
		while True:
			try:
				v = bytearray()
				t = None
				while True:
					d = self.scpi.read(1)
					if d == b",":
						break
					if t is None:
						t = self.timefunc()
					if d in (b"\r", b"\n"):
						eof = True
						break
					v += d

				ret = float(v)
				if ret > 1e37:
					ret = float("NaN")
				yield t, ret
				if eof:
					return
			except KeyboardInterrupt:
				self.abort()
				break

	def _ask(self, what):
		while True:
			try:
				return self.scpi.ask(what)
			except TimeoutError as e:
				logger.exception("Retrying: %s", e)

	def immediate_read(self):
		"""
		Perform single reading, using *TRG and READ?
		"""
		#self.scpi.write("*TRG")
		t = self.timefunc()
		v = self._ask("READ?")
		ret = float(v)
		if ret > 1e37:
			ret = float("NaN")
		return t, ret

	def get_both(self):
		l = self.scpi.ask("INIT; FETCH1?; FETCH2?")
		m = re.match(r"(?P<i>\S+);(?P<v>\S+)", l)
		assert m is not None
		return float(m.group("i")), float(m.group("v"))

	def get_one(self):
		l = self.scpi.ask("INIT; FETCH1?")
		m = re.match(r"(?P<i>\S+)", l)
		assert m is not None
		return float(m.group("i"))

	def fetch(self, function=1):
		"""
		"""
		l = self.scpi.ask(f"FETCH{function}?")
		ret = []
		for x in l.split(","):
			if x.endswith("E+37"):
				logger.info("Questionable/out-of-range measurement %s", x)
				x = float("NaN")
			else:
				x = float(x)
			ret.append(x)
		return ret

	def get_ohms(self):
		a = self.scpi.ask("MEAS:RES?")
		b = re.match(r"^'(\S+?)(\r\n)?'$", a)
		c = b.group(1)
		logger.debug("c =",c)
		res = float(c)
		#except ValueError:# as e:
		#		print "Error, value " + a + " not correct"
		#		time.sleep(1)

		logger.debug("res =", res, "ohm")
		time.sleep(.2)
		return res

	def get_volts(self, rng=100):
		a = self.scpi.ask("MEAS:VOLT:DC? %d" % rng)
		a = a.strip().replace("+", "").replace("'", "")
		res = float(a)
		return res

	def get_amps(self):
		a = self.scpi.ask("MEAS:CURR:DC?")
		#print(a)
		a = a.strip().replace("+", "").replace("'", "")
		res = float(a)
		return res

