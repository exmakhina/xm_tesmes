#########################
CapTTY Virtual Multimeter
#########################

This thing can be used to read measurements from a CapTTY capture
file, which is typically being live-updated, but this can also look
in the past...

The capture file should capture entries formatted as follows:

::

   key: 1.1234

So basically ASCII key, colon, decimal value
