import logging

from .multimeter_121gw import *


logger = logging.getLogger(__name__)


bdaddr = os.environ.get("MULTIMETER_121GW_BDADDR",
 "88:6B:0F:81:AA:48",
)

def test_basic():
	with Multimeter(bdaddr) as meter:
		while True:
			res = meter.measure_immediate()
			logger.info("got %s", res)
		logger.info("done")


def test_ext():
	with Multimeter(bdaddr) as meter:
		while True:
			value, extra = meter.measure_immediate_ext()

			class Pouet:
				pass

			v = Pouet()
			for ek, ev in extra.items():
				setattr(v, ek, ev)

			logger.info("Status: %s", {k: v for k, v in v.status.items() if v != 0})
			logger.info("Main: %.6f (%s=%d %d)", v.main_val, v.main_mode, v.main_mode_code, v.main_rng)
			logger.info("Main status: %s", {k: v for k, v in v.main_status.items() if v != 0})
			logger.info("Sub: %.6f (%s=%d %d)", v.sub_val, v.sub_mode, v.sub_mode_code, v.sub_rng)
			logger.info("Sub status: %s", {k: v for k, v in v.sub_status.items() if v != 0})
			logger.info("Bar: %s", "*" * v.bar_value)
			logger.info("Bar status: %s", {k: v for k, v in v.bar_status.items() if v != 0})


def test_voltage_and_current():
	with Multimeter(bdaddr) as meter:
		while True:
			v, i = meter.measure_va_voltage_and_current()
			logger.info("v=%f i=%f", v, i)
		logger.info("done")
