#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Multimeter implementation for the Fluke network multimeter

import io
import logging
import os
import re
import time
import contextlib

with io.open(__file__.replace(".py", ".rst"), "r") as fi:
	__doc__ = fi.read()


logger = logging.getLogger(__name__)


class Multimeter:
	"""
	"""
	def __init__(self, scpi):
		self.scpi = scpi

	def __enter__(self):
		"""
			Performs initialization of the meter.
		"""
		idn = self.scpi.ask("*IDN?")
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		pass

	@contextlib.contextmanager
	def in_remote(self):
		self.init_remote()
		yield
		self.init_local()

	@contextlib.contextmanager
	def in_local(self):
		self.init_local()
		yield
		# Can't go back
		self.init_local()

	def init_remote(self):
		logger.info("Enter remote control")
		#a = self.scpi.write("SYST:REM")

	def init_local(self):
		logger.info("Enter local control")
		a = self.scpi.write("SYST:LOC")

	def _ask(self, what):
		while True:
			try:
				return self.scpi.ask(what)
			except TimeoutError as e:
				logger.exception("Retrying: %s", e)

	def immediate_read(self):
		"""
		Perform single reading, using *TRG and READ?
		"""
		t = time.monotonic()
		v = self._ask("READ?")
		ret = float(v)
		if ret > 1e37:
			ret = float("NaN")
		return t, ret

	def ask_r(self):
		"""
		"""
		self.scpi.write("R?")
		shan = self.scpi.read(2)
		if shan[0:1] != b'#':
			raise RuntimeError(shan)
		n = int(shan[1:2])
		count = self.scpi.read(n)
		c = int(count) + 1
		ret = []
		if c == 0:
			return ret
		l = self.scpi.read(c)
		for x in l.decode().split(","):
			if x == "\n":
				continue
			if x.endswith("E+37"):
				logger.info("Questionable/out-of-range measurement %s", x)
				x = float("NaN")
			else:
				x = float(x)
			ret.append(x)
		return ret

	def get_ohms(self):
		a = self.scpi.ask("MEAS:RES?")
		b = re.match(r"^'(\S+?)(\r\n)?'$", a)
		c = b.group(1)
		logger.debug("c =",c)
		res = float(c)
		#except ValueError:# as e:
		#		print "Error, value " + a + " not correct"
		#		time.sleep(1)

		logger.debug("res =", res, "ohm")
		time.sleep(.2)
		return res

	def get_volts(self, rng=100):
		a = self.scpi.ask("MEAS:VOLT:DC? %d" % rng)
		a = a.strip().replace("+", "").replace("'", "")
		res = float(a)
		return res

	def get_amps(self):
		a = self.scpi.ask("MEAS:CURR:DC?")
		#print(a)
		a = a.strip().replace("+", "").replace("'", "")
		res = float(a)
		return res

