import logging

from .multimeter_sigrok import *


logger = logging.getLogger(__name__)


def test():
	option_str = os.environ.get("MULTIMETER_SIGROK",
	 "eevblog-121gw:conn=bt/ble122/88-6B-0F-81-AA-48",
	)
	with Multimeter(option_str) as meter:
		logger.info("entered")
		while True:
			logger.info("measure?")
			res = meter.measure_immediate()
			logger.info("measure! %s", res)
		logger.info("exit")
