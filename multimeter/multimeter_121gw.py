#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Multimeter implementation for the 121GW multimeter, using bleak for BLE comm

import io
import os
import time, socket, re, datetime, sys
import logging
import sys
import asyncio
import struct
import logging
import queue
import threading

import bleak


with io.open(__file__.replace(".py", ".rst"), "r") as fi:
	__doc__ = fi.read()


logger = logging.getLogger(__name__)


class Multimeter():
	"""
	"""
	def __init__(self, bdaddr):
		self.queue = queue.Queue()
		self.bdaddr = bdaddr

	def __enter__(self):
		self.running = True
		self._last = b""
		self.t = threading.Thread(target=self.run)
		self.t.start()
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		self.running = False
		self.t.join()

	def measure_immediate_ext(self):
		now = time.monotonic()

		while True:
			if not self.running:
				raise RuntimeError("stopped")

			x = self.queue.get()
			if x is None:
				raise RuntimeError("stopped")

			t, v = x

			if t < now:
				continue

			return v["main_val"], v


	def measure_immediate(self):
		v, extra = self.measure_immediate_ext()
		return v


	def measure_va_voltage_and_current(self):
		v, extra = self.measure_immediate_ext()
		v = extra
		p = v["main_val"]

		sub = v["sub_val"]

		logger.info("v=%s", v)

		if v["sub_mode"] in ("DC A", "DC mA"):
			i = sub
			v = p / i
			return v, i
		elif v["sub_mode"] in ("DC V", "DC mV"):
			v = sub
			i = p / v
			return v, i
		else:
			raise NotImplementedError(v["sub_mode"])

	async def _run(self):
		client = bleak.BleakClient(self.bdaddr)
		try:
			while True:
				try:
					await client.connect()
					break
				except bleak.exc.BleakDeviceNotFoundError as e:
					logger.info("Device not found...")
				except bleak.exc.BleakError as e:
					logger.info("Retrying (%s)", e)
				except bleak.exc.BleakDBusError as e:
					logger.info("E: %s %s", e, dir(e))
					if "le-connection-abort-by-local" in str(e):
						continue
					raise

			logger.info("Connected")
			uid = "e7add780-b042-4876-aae1-112855353cc1"
			await client.start_notify(uid, self.callback)
			while self.running:
				await asyncio.sleep(1.0)
			await client.stop_notify(uid)
		except Exception as e:
			logger.exception("Exception: %s", e)
		finally:
			self.queue.put(None)
			await client.disconnect()

	def run(self):
		asyncio.run(self._run())

	def callback(self, x, y):

		#sys.stdout.write("\x1B[31;1m{}\x1B[0m".format(y.hex()))
		#sys.stdout.flush()

		now = time.monotonic()

		logger.debug("Got: %s", y.hex())
		#return

		if y == b"\xf2":
			self._last = b""
		else:
			y = self._last + y

		while len(y) >= 19:

			pkt = y[:19]
			logger.debug("Processing one packet: %s", pkt.hex())
			y = y[19:]

			e = struct.unpack(
			 ">B"
			 "B" # year
			 "B" # month+sn4
			 "B" # sn3 sn2
			 "B" # sn1 sn0
			 "B" # main mode
			 "B" # main range
			 "H" # main val
			 "B" # sub
			 "B" # subrange
			 "H" # subval
			 "B" # barstatus
			 "B" # barvalue
			 "B" # sts
			 "B" # sts2
			 "B" # sts3
			 "B" # chk
			 , pkt)

			chk_mes = 0
			for b in pkt[:-1]:
				chk_mes ^= b

			if chk_mes != pkt[-1]:
				logger.info("Checksum sez %s computed %s", pkt[-1], chk_mes)
				continue

			#print(e)

			(
				f2,
				year, month_sn4, sn3_sn2, sn1_sn0,
				main_mode, main_range, main_val,
				sub_mode, sub_range, sub_val,
				bar_status, bar_value,
				sts1, sts2, sts3,
				chk
			) = e

			if f2 != 0xf2:
				logger.error("Wrong start byte %s", pkt)

			month = month_sn4 >> 4
			sn4 = month_sn4 & 0xf
			sn3 = sn3_sn2 >> 4
			sn2 = sn3_sn2 & 0xf
			sn1 = sn1_sn0 >> 4
			sn0 = sn1_sn0 & 0xf

			year += 2000

			logger.debug(f"- {year:04d}-{month:02d} {sn4}{sn3}{sn2}{sn1}{sn0}")

			mode_map = {
			 0: "loz",
			 1: "DC V",
			 2: "AC V",
			 3: "DC mV",
			 4: "AC mV",
			 5: "°C",
			 6: "Hz",
			 7: "ms",
			 8: "% duty",
			 10: "res",
			 11: "diode",
			 12: "farad",
			 13: "AC uVA",
			 14: "AC mVA",
			 15: "AC VA",
			 16: "AC uA",
			 17: "DC uA",
			 18: "AC mA",
			 19: "DC mA",
			 20: "AC A",
			 21: "DC A",
			 22: "DC uVA",
			 23: "DC mVA",
			 24: "DC VA",
			 100: "°C",
			 105: "°F",
			 110: "bat",
			 120: "APO on",
			 125: "APO off",
			 130: "Year",
			 135: "Date",
			 140: "Time",
			 150: "B_volt",
			 156: "burden_volt_off",
			 160: "LCD",
			 180: "dBm",
			 190: "ival",
			}

			main_mode_name = mode_map[main_mode]
			sub_mode_name = mode_map[sub_mode]

			if sub_mode == 0:
				sub_mode_name = "ln 0"

			if sub_mode_name == "°C":
				sub_val *= 0.1

			sub_rng = sub_range & 0x7


			if "A" in sub_mode_name:
				if sub_rng == 3:
					sub_val *= 1e-3
				elif sub_rng == 2:
					sub_val *= 1e-5
				else:
					logger.debug("sub %s rng %d val %f", sub_mode_name, sub_rng, sub_val)
			elif "V" in sub_mode_name:
				if sub_rng == 3:
					sub_val *= 1e-3
				else:
					logger.debug("sub %s rng %d val %f", sub_mode_name, sub_rng, sub_val)
			else:
				logger.debug("sub %s rng %d val %f", sub_mode_name, sub_rng, sub_val)

			main_val |= (main_mode >> 6) << 16

			main_val *= -1 if (main_range & (1<<6)) != 0 else 1

			main_rng = main_range & 0xf

			if "VA" in main_mode_name:
				if main_rng == 1:
					main_val *= 1e-3
				elif main_rng == 2:
					main_val *= 1e-3
				elif main_rng == 3:
					main_val *= 1e-2
			elif "mV" in main_mode_name:
				if main_rng == 0:
					main_val *= 1e-6
					main_acc = "+-0.1% + 10 µV"
				if main_rng == 1:
					main_val *= 1e-5
					main_acc = "+-0.1% + 100 µV"
			elif "V" in main_mode_name:
				if main_rng == 0:
					main_val *= 1e-4
					main_acc = "+-0.05% + 0.0005 V"
				if main_rng == 1:
					main_val *= 1e-3
					main_acc = "+-0.05% + 0.005 V"
				if main_rng == 2:
					main_val *= 1e-2
					main_acc = "+-0.05% + 0.05 V"
				if main_rng == 3:
					main_val *= 1e-1
					main_acc = "+-0.1% + 1 V"
			elif "mA" in main_mode_name:
				if main_rng == 0:
					main_val *= 1e-6
				if main_rng == 1:
					main_val *= 1e-5
			elif "A" in main_mode_name:
				if main_rng == 0:
					main_val *= 1e-5
				if main_rng == 1:
					main_val *= 1e-4
				if main_rng == 2:
					main_val *= 1e-3
				if main_rng == 3:
					main_val *= 1e-2
			else:
				logger.warning("Not implemented: %s/%s", main_mode, main_rng)

			if (main_range & (1<<7)) != 0:
				main_val = float("NaN")

			status = {
			 "°C": ((sts1 >> 7) & 1),
			 "1kHz": ((sts1 >> 6) & 1),
			 "1ms": ((sts1 >> 5) & 1),
			 "DC+AC": ((sts1 >> 4) & 1),
			 "AUTO": ((sts1 >> 2) & 1),
			 "APO": ((sts1 >> 1) & 1),
			 "BAT": ((sts1 >> 0) & 1),
			 "°F": ((sts2 >> 7) & 1),
			 "BT": ((sts2 >> 6) & 1),
			 "<": ((sts2 >> 5) & 1),
			 "REL": ((sts2 >> 4) & 1),
			 "dBm": ((sts2 >> 3) & 1),
			 "Min-max": ((sts2 >> 0) & 7),
			 "TEST": ((sts3 >> 6) & 1),
			 "MEM": ((sts3 >> 4) & 3),
			 "A-HOLD": ((sts3 >> 2) & 3),
			 "AC": ((sts3 >> 1) & 1),
			 "DC": ((sts3 >> 0) & 1),
			}

			main_status = {
			 "OFL": (main_range >> 7) & 1,
			 "-": (main_range >> 6) & 1,
			 "°C": (main_range >> 5) & 1,
			 "°F": (main_range >> 4) & 1,
			}

			sub_status = {
			 "OFL": (sub_range >> 7) & 1,
			 "-": (sub_range >> 6) & 1,
			 "k": (sub_range >> 5) & 1,
			 "Hz": (sub_range >> 4) & 1,
			}

			bar_status = {
			 "USE": (bar_status >> 4) & 1,
			 "0~150": (bar_status >> 3) & 1,
			 "-": (bar_status >> 2) & 1,
			 "1000/500": (bar_status >> 0) & 3,
			}

			res = dict(
			 t=now,
			 status=status,
			 main_val=main_val,
			 main_rng=main_rng,
			 main_mode=main_mode_name,
			 main_mode_code=main_mode,
			 main_status=main_status,
			 sub_val=sub_val,
			 sub_rng=sub_rng,
			 sub_mode=sub_mode_name,
			 sub_mode_code=sub_mode,
			 sub_status=sub_status,
			 bar_value=bar_value,
			 bar_status=bar_status,
			)

			self.queue.put((now, res))

		self._last = y

		logger.debug("Remaining %s", self._last.hex())
