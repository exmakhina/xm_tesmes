#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Multimeter implementation for captty live capture

import sys, io, os
import re
import logging
import time


from exmakhina.captty.captty import (
 CaptureReader,
 generate_lines,
)


with io.open(__file__.replace(".py", ".rst"), "r") as fi:
	__doc__ = fi.read()


logger = logging.getLogger(__name__)


class Multimeter():
	def __init__(self, path, key="main", poll_time=1.0):
		self.path = path
		self.key = key
		self.poll_time = poll_time
		self.now = None

	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		pass

	def measure_immediate_ext(self):
		if self.now is not None:
			now = self.now
		else:
			now = time.time()
		kw = dict()
		if self.path.endswith(".mdb"):
			kw.update(beg=now)

		with CaptureReader(self.path, **kw) as reader:
			logger.info("Reading immediate %s", now)
			while True:
				for ts_beg, ts_end, line in generate_lines(reader, ts_beg=now, eol=b"\n"):
					line = line.decode().rstrip()
					m = re.match(r"^(?P<k>\S+): (?P<v>\S+)(\s+(?P<u>\S+)(\s+(?P<x>.*))?)?$", line)
					if m is None:
						logger.warning("Unhandled message: %s", line)
						continue
					logger.info("Got %s", m)
					k = m.group("k")
					if k == self.key:
						v = float(m.group("v"))
						extra = dict(
						 ts_beg=ts_beg,
						 ts_end=ts_end,
						)
						try:
							extra["u"] = m.group("u")
						except KeyError:
							pass
						try:
							extra["x"] = m.group("x")
						except KeyError:
							pass

						self.then = ts_end

						return v, extra

				time.sleep(self.poll_time)

	def measure_immediate(self):
		v, extra = self.measure_immediate_ext()
		return v

