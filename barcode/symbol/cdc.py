#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# Userspace driver for Symbol DS6708 in "CDC Host" mode
# SPDX-FileCopyrightText: 2018-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

"""

Notes:

- In CDC mode, the serial port must be open in raw mode (this
  __main__ does it)

- In CDC mode, the DS6708 doesn't provide its S/N.
  When many readers are used, use /dev/serial/by-path


"""

import logging
import select


logger = logging.getLogger(__name__)


def symbol_cdc_read(f, eol=b""):
	"""
	Read a code in CDC mode.

	This is time-based.
	"""

	out = bytearray()
	while True:
		r, w, e = select.select([f], [], [], 0.1)
		if r == []:
			if len(out) == 0:
				continue
			elif out.endswith(eol):
				logger.debug("End of code")
				break
			else:
				logger.warning("Unexpected timeout")

		d = f.read(1)
		logger.info("Read %s", d)
		if not d:
			logger.warning("Unexpected broken pipe")
			break

		out.append(d[0])

	if eol:
		return bytes(out[:-len(eol)])
	else:
		return bytes(out)

