import logging
import contextlib
import os
import time

from ..psu.psu_korad_ka3305p import PSU
from ..psu.psu_channel import PSU as PSU_Channel
from ..scpi.scpi_command import SCPI
from ..load.load_korad_kel103 import Load
from .battery_psu_load import Battery

logger = logging.getLogger(__name__)


def test():

	cmd_psu = os.environ.get("KA3305P_STDIO_COMMAND",
	 "ssh bbg4xm1 socat stdio open:/dev/serial/by-id/usb-USB_Vir_USB_Virtual_COM_NT2009101400-if00,b9600,rawer",
	)
	cmd_load = os.environ.get("KEL103_STDIO_COMMAND",
	 "ssh bbg4xm1 socat stdio open:/dev/serial/by-id/usb-Nuvoton_KORAD_USB_Mode_000009A60000-if00,b115200,rawer",
	)

	with contextlib.ExitStack() as stack:
		scpi_load = SCPI(cmd_load, eol_rx=b"\n", eol_tx=b"\n")
		stack.enter_context(scpi_load)
		load = Load(scpi_load)
		stack.enter_context(load)

		scpi_psu = SCPI(cmd_psu, eol_rx=b"\x00", eol_tx=b"")
		stack.enter_context(scpi_psu)
		scpi_psu.ser._default_timeout = 2
		psu = PSU(scpi_psu)
		stack.enter_context(psu)
		psu_ch = PSU_Channel(psu, channel=1)

		bat = Battery(psu_ch, load)

		while True:
			#i = bat.measure_current()
			#logger.info("I: %f", i)
			time.sleep(0.25)

			bat.step()
