import logging
import time


logger = logging.getLogger(__name__)


class Battery:
	"""
	For now this is just a silly simulation of a 4S Li-Po battery
	"""
	def __init__(self, psu, load, diode_drop=0.43):
		self.psu = psu
		self.load = load
		self.diode_drop = diode_drop
		self.nominal_charge = 0.01 * 3600 # A*s
		self.charge = 0.5 * self.nominal_charge
		self.last_measure = time.time()
		self.last_current = 0

	def measure_current(self):
		"""
		+ means discharge
		"""
		c_psu = self.psu.sense_current()
		c_load = self.load.measure_i()
		return c_psu - c_load


	def step(self):
		now = time.time()
		i = self.measure_current()
		dt = now - self.last_measure
		self.last_measure = now
		self.last_current = i

		dc = i * dt

		self.charge -= dc

		soc = self.charge / self.nominal_charge
		sod = 1 - soc


		v = 4 * 4.2 - ((4 * 4.2) - (4 * 3.3)) * sod

		if v > 4*4.2:
			logger.warning("Overcharge!")
			v = 4*4.2
		if v < 4*3.3:
			logger.warning("Overdischarge!")
			v = 4*3.3

		logger.info("Soc: %6.3f i=%6.3f v=%6.3f", soc, i, v)

		self.load._func = "VOLT"
		self.load._unit = "V"
		self.load.setpoint_set(v)
		self.psu.voltage_setpoint_set(v+self.diode_drop)
