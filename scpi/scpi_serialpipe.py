#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SCPI interface for a SerialPipe

import logging
import signal
import contextlib
import threading

logger = logging.getLogger(__name__)

@contextlib.contextmanager
def defer_signal(signum):
	# Based on https://stackoverflow.com/a/71330357/1319998

	original_handler = None
	defer_handle_args = None

	def defer_handle(*args):
		nonlocal defer_handle_args
		defer_handle_args = args

	# Do nothing if
	# - we don't have a registered handler in Python to defer
	# - or the handler is not callable, so either SIG_DFL where the system
	#   takes some default action, or SIG_IGN to ignore the signal
	# - or we're not in the main thread that doesn't get signals anyway
	original_handler = signal.getsignal(signum)
	if (
	 original_handler is None
	 or not callable(original_handler)
	 or threading.current_thread() is not threading.main_thread()
	):
		yield
		return

	try:
		signal.signal(signum, defer_handle)
		yield
	finally:
		signal.signal(signum, original_handler)
		if defer_handle_args is not None:
			original_handler(*defer_handle_args)


class SCPI:
	"""
	"""
	def __init__(self, ser: "SerialPipe", eol=b"\r\n", eol_tx=None, eol_rx=None):
		"""
		:param ser: SerialPipe, which must be entered
		when entering this object
		"""
		self.ser = ser
		self.lock = threading.RLock()

		if eol_tx is None:
			eol_tx = eol
		if eol_rx is None:
			eol_rx = eol
		self.eol_tx = eol_tx
		self.eol_rx = eol_rx

	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		logger.info("%s %s %s", exc_type, exc_value, exc_traceback)
		pass

	def read(self, n):
		with self.lock:
			return self.ser.read(n)

	def readline(self):
		with self.lock:
			return self.ser.readline(eol=self.eol_rx, timeout=100)

	def write(self, cmd):
		logger.debug("> %s", cmd)
		with self.lock:
			with defer_signal(signal.SIGINT):
				return self.ser.write("{}".format(cmd).encode('ascii') + self.eol_tx)

	def ask(self, cmd):
		logger.debug("> %s", cmd)
		with self.lock:
			with defer_signal(signal.SIGINT):
				self.ser.write("{}".format(cmd).encode('ascii') + self.eol_tx)
				res = self.readline().rstrip().decode('ascii')
		logger.debug("< %s", res)
		return res

