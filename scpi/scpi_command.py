#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SCPI interface over a command-line

import logging
import shlex
import subprocess
import contextlib
import os

from ...serialpipe.serialpipe import SerialPipe
from ...konvini.subprocess import TerminatingPopen

from .scpi_serialpipe import SCPI as Base


logger = logging.getLogger(__name__)


class SCPI(Base):
	"""
	Get SCPI access through a command line
	"""
	def __init__(self, cmd: str, eol=b"\r\n", eol_tx=None, eol_rx=None):
		super().__init__(ser=None, eol=eol, eol_tx=eol_tx, eol_rx=eol_rx)
		self.cmd = cmd
		self.stack = contextlib.ExitStack()

	def __enter__(self):
		stack = self.stack
		stack.__enter__()

		cmd = shlex.split(self.cmd)
		proc = TerminatingPopen(cmd,
		 stdin=subprocess.PIPE,
		 stdout=subprocess.PIPE,
		 bufsize=0,
		 preexec_fn=os.setsid,
		)
		stack.enter_context(proc)

		serial_pipe = SerialPipe(proc.stdin, proc.stdout)
		stack.enter_context(serial_pipe)

		self.ser = serial_pipe
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		return self.stack.__exit__(exc_type, exc_value, exc_traceback)
