
class scpi_common:
	def __init__(self, **kw):
		self._kw = kw

	def __call__(self, cls):

		kw = self._kw
		if kw["star_cls"] is Ellipsis:
			cls.clear_errors = lambda self: self.scpi.write("*CLS")

		if kw["syst_err"] is Ellipsis:
			cls.clear_errors = lambda self: self.scpi.ask("SYST:ERR?")
		else:
			cls.clear_errors = lambda self: self.scpi.ask(kw["syst_err"])

		return cls
