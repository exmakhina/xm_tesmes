# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-tesmes@zougloub.eu>
# SPDX-License-Identifier: LGPL-2.1-or-later

import logging
import typing as t


logger = logging.getLogger(__name__)


class AcqPromise:
	def __init__(self, daq: "DAQ", channels, sps, count):
		self.daq = daq
		self.channels = channels
		self.sps = sps
		self.count = count
		self.t = threading.Thread(target=self.run)
		self.t.start()

	def __call__(self):
		self.t.join()
		return self.data

	def run(self):
		try:
			self.data = self.daq.ain_simple(self.channels, self.sps, self.count)
		except Exception as e:
			logger.info("Not returning data: %s", e)
			self.data = None


class DAQ:
	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		return

	def ain_streaming(self, channelspec: t.Sequence[t.Tuple[int,float,float]], freq):
		pass

	def ain_simple(self, channelspec: t.Sequence[t.Tuple[int,float,float]], sps, count):
		pass

	def ain_simple_promise(self, channels: t.Sequence[t.Tuple[int,float,float]], sps, count):
		"""
		:param channels: list of (channel, vmin, vmax)
		"""
		acq = AcqPromise(self, channels, sps, count)
		return acq
