# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-tesmes@zougloub.eu>
# SPDX-License-Identifier: LGPL-2.1-or-later

import os
import sys
import typing as t
import threading
import logging
import itertools


import numpy as np

from exmakhina.lazystr import LazyBHex

from usb_1608FS import *


logger = logging.getLogger(__name__)


def find_gain(vmin, vmax):
	"""
	Find gain according to signal range.

	Note: this is done based on nominal values and ignores calibration data.
	A more rigorous method could be used, which would use calibration data...
	it might save a bit of accuracy.

	"""
	if -0.3125 <= vmin <= 0.3125 and -0.3125 <= vmax <= 0.3125:
		return usb_1608FS.BP_0_3125V
	if -0.625 <= vmin <= 0.625 and -0.625 <= vmax <= 0.625:
		return usb_1608FS.BP_0_625V
	if -1 <= vmin <= 1 and -1 <= vmax <= 1:
		return usb_1608FS.BP_1_00V
	if -1.25 <= vmin <= 1.25 and -1.25 <= vmax <= 1.25:
		return usb_1608FS.BP_1_25V
	if -2.5 <= vmin <= 2.5 and -2.5 <= vmax <= 2.5:
		return usb_1608FS.BP_2_50V
	if -5 <= vmin <= 5 and -5 <= vmax <= 5:
		return usb_1608FS.BP_5_00V
	if -10 <= vmin <= 10 and -10 <= vmax <= 10:
		return usb_1608FS.BP_10_00V
	return ValueError(f"No gain available for {vmin:.3f}-{vmax:.3f}")


