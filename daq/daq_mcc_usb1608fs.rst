################################
Measurement Computing USB-1608FS
################################


:Brief: https://digilent.com/reference/_media/daq-and-datalogging/specs/ds-usb-1608fs.pdf
:Manual: https://files.digilent.com/manuals/USB-1608FS.pdf


Performance
###########

Max speed in streaming (using immediate transfer mode):

- 1 channel: 25 k/s
- 2 channels: 15 k/s
- 4 channels: 7.5 k/s
- 8 channels: 3.8 k/s

Max aggregate is quoted at 32k sps but I couldn't reach this;
the device is not working reliably when close to max speed.

